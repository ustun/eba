

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const axios = require('axios');

app.use(bodyParser.json());


var db = {
    sorular: [
	{id: 1, soru: 'Nasilsin?'},
	{id: 2, soru: 'Nerelisin?'},
    ]
};

app.get('/api/soru', function (req, res) {
    res.send({data: db.sorular});
});

app.get('/api/soru/:id', function (req, res) {
    var soru = db.sorular.find(soru => soru.id === Number(req.params.id));
    res.send({data:soru});
});

app.post('/api/soru', function (req, res) {

    var soru = req.body; // {soru: "dolar ne kadar?"}

    soru.id = db.sorular.length;

    db.sorular.push(soru);

    res.send({data: soru});


});

app.delete('/api/soru/:id', function (req, res) {

    db.sorular = db.sorular.filter(soru => soru.id !== Number(req.params.id));

    res.send({deleted: true});
});


app.get('/api/repos/', function (req, res) {

    axios.get('https://api.github.com/users/ustun/repos')
	.then(data => res.send(data))
	.catch(err => res.send({error: true, err}));


});



app.listen(1235);
