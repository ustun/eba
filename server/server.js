// nodemon --harmony-async-await server.js

const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const axios = require("axios");
var { renderToString } = require("react-dom/server");
const faker = require("faker");
var uuid = require("uuid");
const _ = require("lodash");

// var {appReducer} = require('./appReducer');

var React = require("React");

class Merhaba extends React.Component {
  render() {
    return React.createElement("div", null, "Merhaba", this.props.isim);
  }
}

class AnaSayfa extends React.Component {
  render() {
    return React.createElement(Merhaba, { isim: "Ali" });
  }
}

app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,POST,DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

var db = {
  sorular: [{ id: 1, soru: "Nasilsin?" }, { id: 2, soru: "Nerelisin?" }]
};

var count = 0;

app.get("/api/n-notifications", function(req, res) {
  res.send({ data: 0 });
});

app.get("/", function(req, res) {
  //     var store = createStore(appReducer);
  //     var userDetails = await db.getUserDetails(req.user.id);
  //     store.dispatch({type: 'SET_USER', payload: userDetails});
  //     var X = React.createElement(Provider, {store: store}, AnaSayfa);
  //     //
  //     // store = createStore(myReducer)
  //     // store.dispatch({type: 'SET_INITIAL_STATE', payload: window.__INITIAL_STATE});
  //     var html = renderToString(X);
  //     res.send(`<html>
  // <div id="root">${html}</div>
  // <script>
  // window.__INITIAL_STATE__ = ${store.getState()};
  // </script>
  // </html>`
  // 	    });
});

app.get("/api/sayac", function(req, res) {
  res.send({ count });
});

app.post("/api/sayac", function(req, res) {
  count++;
  res.send({ count });
});

app.delete("/api/sayac/", function(req, res) {
  count = 0;
  res.send({ count });
});

var randomUser = function() {
  return {
    id: uuid(),
    fullName: faker.name.findName()
  };
};

var randomRelatedContent = function() {
  return {
    id: uuid(),
    baslik: faker.lorem.sentences(1),
    user: randomUser(),
    dateCreated: faker.date.past()
  };
};

function sleep(sure) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      resolve();
    }, sure);
  });
}

var contents = _.times(50, randomRelatedContent);

app.get("/api/related-contents", async function(req, res) {
  await sleep(300);

  var filteredContents = req.query.q
    ? contents.filter(content =>
        content.baslik.toLowerCase().includes(req.query.q.toLowerCase())
      )
    : contents;

  // console.log(req.query.baslangicIndexi)

  var baslangicIndexi = Number(req.query.baslangicIndexi) || 0;

  var slicedContents = filteredContents.slice(
    baslangicIndexi,
    baslangicIndexi + 10
  );

  res.send({
    data: slicedContents,
    meta: {
      queryParam: req.query.q,
      nTotalResults: contents.length
    }
  });
});

app.get("/api/soru", function(req, res) {
  res.send({ data: db.sorular });
});

app.get("/api/soru/:id", function(req, res) {
  var soru = db.sorular.find(soru => soru.id === Number(req.params.id));
  res.send({ data: soru });
});

app.post("/api/soru", function(req, res) {
  var soru = req.body; // {soru: "dolar ne kadar?"}

  soru.id = db.sorular.length;

  db.sorular.push(soru);

  res.send({ data: soru });
});

app.delete("/api/soru/:id", function(req, res) {
  db.sorular = db.sorular.filter(soru => soru.id !== Number(req.params.id));

  res.send({ deleted: true });
});

app.get("/api/repos/:username", async function(req, res) {
  try {
    var x = await axios.get(
      "https://api.github.com/users/" + req.params.username
    );
    res.send(x.data);
  } catch (e) {
    res.send({ error: true });
  }
});

async function doit() {
  var x = await axios.get("http://localhost:1235/api/soru");

  console.log(x.data.data);
}

doit();

app.listen(1237);
