import React, { Component } from "react";
import { Provider } from "react-redux";
import store from "../store";
import MainContainer from "./mainContainer";

export default class Main extends Component {
  render() {
    return (
      <Provider store={store}>
        <MainContainer />
      </Provider>
    );
  }
}
