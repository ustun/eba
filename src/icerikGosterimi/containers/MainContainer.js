import React, { Component } from "react";
import { connect, Provider } from "react-redux";
import AppPure from "../components/appPure";
import { sleep } from "../../utils";
import store from "../store";
import axios from "axios";
import { getNotificationsString } from "../models";
import * as constants from "../constants";

function mapStateToProps(appState) {
  return {};
}

function mapDispatchToProps() {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(AppPure);
