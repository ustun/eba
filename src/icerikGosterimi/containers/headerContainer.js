import React, { Component } from "react";
import { connect, Provider } from "react-redux";
import AppPure from "../components/appPure";
import { sleep } from "../../utils";
import store from "../store";
import axios from "axios";
import { getNotificationsString } from "../models";
import * as constants from "../constants";
import Header from "../components/header";

function mapStateToProps(appState) {
  // debugger
  return {
    nUnreadMessages: appState.nUnreadMessages,
    nNotifications: getNotificationsString(appState),
    notificationsFetched: appState.notificationsFetched,
    photoUrl: appState.user.avatar
  };
}

function mapDispatchToProps() {
  return {
    getNotifications: async function getNo1tifications() {
      await sleep(3000);
      var response = await axios.get("/api/n-notifications");
      store.dispatch({
        type: constants.SET_N_NOTIFICATIONS,
        payload: response.data.data
      });
    }
  };
}

const HeaderContainer = connect(mapStateToProps, mapDispatchToProps)(Header);
export default HeaderContainer;
