import store from "../store";
import Api from "../apis/ilgiliIcerik";
import SearchContentArea from "../components/searchContentArea";
import { connect } from "react-redux";

function mapStateToPropsSearch(appState) {
  return {
    arama: appState.arama
  };
}

function mapDispatchToPropsSearch(dispatch, getState) {
  var functions = {
    loadMore() {
      functions.fetchData(
        store.getState().getIn(["arama", "metin"]),
        store.getState().getIn(["arama", "baslangicIndexi"])
      );
    },

    onFormSubmit(event) {
      event.preventDefault();

      dispatch({
        type: "RESET_RELATED_CONTENTS"
      });

      functions.loadMore();
    },

    setMetin(event) {
      dispatch({
        type: "SET_METIN",
        payload: event.target.value
      });
    },

    async fetchData(metin, baslangicIndexi) {
      if (store.getState().arama.fetchInProgress) {
        return;
      }

      var response;
      dispatch({
        type: "SET_FETCH_IN_PROGRESS",
        payload: true
      });
      try {
        response = await Api.getRelatedContents(metin, baslangicIndexi);
      } catch (e) {
        console.error(e);
      }

      if (response) {
        if (response.status == "200") {
          dispatch({
            type: "LOAD_MORE_RELATED_CONTENTS",
            payload: response.data.data
          });
        } else {
          // save failure
        }
      }

      dispatch({
        type: "SET_FETCH_IN_PROGRESS",
        payload: false
      });
    }
  };

  return functions;
}

export default connect(mapStateToPropsSearch, mapDispatchToPropsSearch)(
  SearchContentArea
);
