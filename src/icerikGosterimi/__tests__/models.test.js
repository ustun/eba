import Api from "../apis/ilgiliIcerik";
import {
  convertIcerikToImmutable,
  convertIceriklerToImmutable,
  EbaUygulama,
  User,
  getNotificationsString
} from "../models";

import Immutable from "immutable";

it("user", () => {
  const user = User();
  expect(user.set("avatar", "x").avatar).toBe("x");

  expect(() => user.set("sinif", "x")).toThrow();
  expect(() => user.set("firstName", "x")).not.toThrow();
});

test("eba uygulama", () => {
  var app = EbaUygulama();

  expect(app.user.firstName).toBe("Default");
  expect(app.getIn(["user", "firstName"])).toBe("Default");

  expect(() => (app.user.firstName = "Ali")).toThrow();

  expect(() => app.setIn(["user", "firstName"], "Ali")).not.toThrow();

  var app2 = app.setIn(["user", "firstName"], "Ali");

  expect(app.user.firstName).toBe("Default");
  expect(app2.user.firstName).toBe("Ali");

  expect(getNotificationsString(EbaUygulama())).toBe("0");
  expect(getNotificationsString(EbaUygulama().set("nNotifications", 10))).toBe(
    "10"
  );
});

test("icerik json to immutable record", () => {
  var payload = [
    {
      id: "5a63d4a3-cef9-4341-a39f-108ccbb9b8a5",
      baslik: "Autem et vero eius labore.",
      user: {
        id: "6c487f6e-76a5-43d5-9b88-6628845c0cf2",
        fullName: "Emie Lang"
      },
      dateCreated: "2017-03-15T22:12:06.963Z"
    },
    {
      id: "6039ace1-027f-408b-acf2-b4507a78c56b",
      baslik: "Ullam accusamus pariatur.",
      user: {
        id: "13a6134d-4e5b-41cb-a190-0507dcef1e1c",
        fullName: "Kali Hauck"
      },
      dateCreated: "2016-08-16T07:10:43.750Z"
    }
  ];
  var immutableIcerikler = convertIceriklerToImmutable(payload);

  expect(immutableIcerikler.count()).toBe(2);

  expect(immutableIcerikler.get(0).baslik).toBeTruthy();

  console.log(immutableIcerikler.get(0).baslik);

  var icerik = {
    id: "6039ace1-027f-408b-acf2-b4507a78c56b",
    baslik: "Ullam accusamus pariatur.",
    user: {
      id: "13a6134d-4e5b-41cb-a190-0507dcef1e1c",
      fullName: "Kali Hauck"
    },
    dateCreated: "2016-08-16T07:10:43.750Z"
  };

  var icerikImmutable = convertIcerikToImmutable(icerik);

  expect(icerik.user.fullName).toBe("Kali Hauck");

  console.log(icerik.user instanceof User);

  var m = Immutable.Map({
    name: "Ustun"
  });
  var r = Immutable.Record({
    name: "Ustun"
  })();

  console.log(m.get("name"));

  console.log(r.name);
});

// test("api", async () => {
//   var response = await Api.getRelatedContents();
//   // expect(response).toBeTruthy();
//   expect(response.data.data.length).toBe(10);
// });
