import Immutable, { Record, List, Map, fromJS } from "immutable";

export const User = Record({
  firstName: "Default",
  lastName: "",
  okul: "",
  avatar: "img/content/profile-img.jpg"
});

// {} -> Map
// [] -> List
// class -> Record

const JSONIcerik = {};

export const IcerikOlusturucu = Record({
  id: null,
  fullName: ""
});

export const IcerikPaketi = Record({
  kucukResim: "",
  icerik: Map(),
  user: IcerikOlusturucu()
});

export const IlgiliIcerik = Record({
  id: 0,
  kucukResim: "",
  baslik: "",
  user: IcerikOlusturucu(),
  dateCreated: new Date()
});

var icerikler = List();

// for (var i = 0 ; i < 10; i++) {
//     icerikler = icerikler.push(IlgiliIcerik().merge(Map({
// 	id: i,
// 	kucukResim: 'eba/eba-v2017-arayuz/img/img1.jpg',
// 	baslik: i % 3 === 0 ? 'baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik baslik ': 'Baslik ' + i,
// 	user: IcerikOlusturucu().set('fullName', 'Ahmet Beyaz'),
// 	dateCreated: new Date()
//     })));
// }

export const DersAkisi = Record({
  icerikPaketleri: List() // List<IcerikPaketi>
});

export const Arama = Record({
  metin: "",
  filtreler: List(),
  icerikler: icerikler,
  nIcerik: 0,
  fetchInProgress: false,
  fetchComplete: false,
  baslangicIndexi: 0,
  hasMore: true
});

export const EbaUygulama = Record({
  nUnreadMessages: 10,
  nNotifications: 0,
  notificationsFetched: false,
  user: User(),
  dersAkisi: DersAkisi(),
  arama: Arama()
});

export function getNotificationsString(app) {
  return app.nNotifications > 99 ? "99+" : String(app.nNotifications);
}

export const EbaUygulama2 = EbaUygulama().merge(
  Map({
    nUnreadMessages: 1280,
    user: User().set("firstName", "Ali")
  })
);

export const EbaUygulama3 = EbaUygulama2.merge(
  Map({
    nUnreadMessages: 128,
    arama: Arama().set("metin", "deneme")
  })
);

export function convertIcerikToImmutable(icerikJS) {
  return IlgiliIcerik().merge(fromJS(icerikJS));
}

export function convertIceriklerToImmutable(iceriklerJS) {
  // iceriklerJS.map(icerikJS => convertIcerikToImmutable(icerikJS));

  return List.of(...iceriklerJS.map(convertIcerikToImmutable));
  // Array of Icerik Record

  // List of Icerik Record
}
