import axios from "axios";

export default {
  getNumberOfNotifications() {
    return axios.get("/api/n-notifications");
  }
};
