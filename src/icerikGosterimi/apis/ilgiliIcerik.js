import axios from "axios";
import queryString from "query-string";

export default {
  getRelatedContents(metin = "", baslangicIndexi = 0) {
    var sonEk = queryString.stringify({
      q: metin,
      baslangicIndexi: baslangicIndexi
    });
    return axios.get("/api/related-contents?" + sonEk);
  }
};
