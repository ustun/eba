import React, { Component } from "react";
import SearchContentAreaKonteyner from "../containers/searchContainer";
import Header from "../containers/headerContainer";

export default class AppPure extends Component {
  render() {
    return (
      <div>
        <Header />

        <main>
          <div className="lesson-bar">
            <div className="container">
              <div className="date-and-package-list">
                <div className="lesson-date">
                  <div className="current-day-number">17</div>
                  <div className="current-day-other">
                    <div className="current-month-year">
                      Nisan
                      2017
                    </div>
                    <div className="current-day-name">
                      Perşembe
                    </div>
                  </div>
                </div>
                <div className="lesson-package-list-area">
                  <ul className="list-inline lesson-package-list">
                    <li className="active-package">
                      <a
                        href="#"
                        className="lesson-package-link"
                        title="Paket 1"
                      >
                        <span className="lesson-package-name">
                          Paket
                          1
                        </span>
                        <div className="lesson-package-thumb">
                          <img
                            src="http://placehold.it/60x40/cccc00/ffffff?"
                            alt="Paket 1"
                          />
                        </div>
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="lesson-package-link"
                        title="Paket 2"
                      >
                        <span className="lesson-package-name">
                          Paket
                          2
                        </span>
                        <div className="lesson-package-thumb">
                          <img
                            src="http://placehold.it/60x40/ff6600/ffffff"
                            alt="Paket 2"
                          />
                        </div>
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="lesson-package-link"
                        title="Paket 3"
                      >
                        <span className="lesson-package-name">
                          Paket
                          3
                        </span>
                        <div className="lesson-package-thumb">
                          <img
                            src="http://placehold.it/60x40/66ccff/ffffff"
                            alt="Paket 3"
                          />
                        </div>
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="lesson-package-link"
                        title="Paket 4"
                      >
                        <span className="lesson-package-name">
                          Paket
                          4
                        </span>
                        <div className="lesson-package-thumb">
                          <img
                            src="http://placehold.it/60x40/919191/ffffff"
                            alt="Paket 4"
                          />
                        </div>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="lesson-code-and-menu-area">
                <div className="lesson-code-title">
                  Derse
                  bağlanmak
                  için:
                </div>
                <div className="lesson-code">45a7</div>
                <div id="o-wrapper" className="o-wrapper classroom-menu">
                  <a id="c-button--slide-right" className="classroom-menu-link">
                    <i className="fa fa-list" />
                  </a>
                </div>
                <nav
                  id="c-menu--slide-right"
                  className="c-menu c-menu--slide-right"
                >
                  <button className="c-menu__close">
                    <i className="fa fa-close" />Kapat
                    →
                  </button>
                  <ul className="c-menu__items">
                    <li>
                      <a href="#" className>
                        Liste
                        1
                      </a>
                    </li>
                    <li>
                      <a href="#" className>
                        Liste
                        2
                      </a>
                    </li>
                    <li>
                      <a href="#" className>
                        Liste
                        3
                      </a>
                    </li>
                    <li>
                      <a href="#" className>
                        Liste
                        4
                      </a>
                    </li>
                    <li>
                      <a href="#" className>
                        Liste
                        5
                      </a>
                    </li>
                  </ul>
                </nav>
                <div className="clearfix" />
              </div>
              <div className="clearfix" />
            </div>
          </div>
          {/* lesson-bar */}
          <div className="package-player-area full-screen-player">
            <div className="container">
              <div className="package-slide-dots">
                <ul>
                  <li>
                    <a href="#" />
                  </li>
                  <li>
                    <a href="#" />
                  </li>
                  <li>
                    <a href="#" />
                  </li>
                  <li>
                    <a href="#" />
                  </li>
                  <li>
                    <a href="#" />
                  </li>
                </ul>
              </div>
              <div className="package-content-list-area">
                <h3 className="package-content-title">
                  Geniş
                  Açı
                </h3>
                <div className="package-content-list">
                  <ul className="list-unstyled">
                    <li className="package-content-item active-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/FFC107/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          Lorem
                          Ipsum
                          Dolor
                          Sit
                          Amet
                        </h4>
                        <span className="package-content-item-type">
                          Konu
                          Anlatımı
                        </span>
                      </a>
                    </li>
                    <li className="package-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/ba68c8/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          İçerik
                          Başlığı
                        </h4>
                        <span className="package-content-item-type">
                          Etkileşim
                        </span>
                      </a>
                    </li>
                    <li className="package-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/9ccc65/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          İçerik
                        </h4>
                        <span className="package-content-item-type">
                          Simülasyon
                        </span>
                      </a>
                    </li>
                    <li className="package-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/4dd0e1/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          Konu
                          Değerlendirmesi
                        </h4>
                        <span className="package-content-item-type">
                          Sınav
                        </span>
                      </a>
                    </li>
                    <li className="package-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/ba68c8/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          İçerik
                          Başlığı
                        </h4>
                        <span className="package-content-item-type">
                          Etkileşim
                        </span>
                      </a>
                    </li>
                    <li className="package-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/9ccc65/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          İçerik
                        </h4>
                        <span className="package-content-item-type">
                          Simülasyon
                        </span>
                      </a>
                    </li>
                    <li className="package-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/4dd0e1/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          Konu
                          Değerlendirmesi
                        </h4>
                        <span className="package-content-item-type">
                          Sınav
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="package-content-footer">
                  <a href="#" className="send-classroom-btn">
                    Sınıfa
                    Gönder
                  </a>
                </div>
              </div>
              <div className="package-player full-width-package-player">
                <img
                  src="img/content/video-player.jpg"
                  alt="Player"
                  width={1140}
                  height={600}
                />
              </div>
              <div className="package-player-controls">
                <div className="package-player-prev-btn">
                  <a href="#" className="prev arrows">
                    <i className="fa fa-arrow-left" />
                  </a>
                </div>
                <div className="package-player-next-btn">
                  <a href="#" className="next arrows">
                    <i className="fa fa-arrow-right" />
                  </a>
                </div>
              </div>
              <div className="clearfix" />
              <div className="package-content-detail">
                <div className="package-content-detail-topic">
                  <span>İçerik1</span>
                  <span className="seperator">|</span>
                  <span>
                    Ahmet
                    Beyaz
                  </span>
                </div>
                <div className="package-content-detail-button">
                  <button
                    id="detail-toggle-btn"
                    type="button"
                    title="Ayrıntılar"
                  >
                    <i className="fa fa-info" />
                  </button>
                  <span>
                    <i className="fa fa-sort-desc" />
                  </span>
                  <span>
                    <i className="fa fa-sort-asc" style={{ display: "none" }} />
                  </span>
                </div>
                <div className="package-content-send-btn">
                  <a href="#" className="send-classroom-btn">
                    Sınıfa
                    Gönder
                  </a>
                </div>
              </div>
              <div className="clearfix" />
              <div className="package-content-comments">
                <div className="package-rating">
                  <div className="package-rating-teach">
                    <ul>
                      <li><i className="fa fa-star" /></li>
                      <li><i className="fa fa-star" /></li>
                      <li><i className="fa fa-star" /></li>
                      <li><i className="fa fa-star-half" /></li>
                      <li><i className="fa fa-star-o" /></li>
                    </ul>
                    <span>
                      12
                      Öğretmen
                    </span>
                  </div>
                  <div className="package-rating-student">
                    <ul>
                      <li><i className="fa fa-star" /></li>
                      <li><i className="fa fa-star" /></li>
                      <li><i className="fa fa-star" /></li>
                      <li><i className="fa fa-star-half" /></li>
                      <li><i className="fa fa-star-o" /></li>
                    </ul>
                    <span>
                      75
                      Öğrenci
                    </span>
                  </div>
                </div>
                <div className="package-rating-bars">
                  <span style={{ width: 200 }}> </span>
                  <span style={{ width: 180 }}> </span>
                  <span style={{ width: 150 }}> </span>
                  <span style={{ width: 120 }}> </span>
                  <span style={{ width: 170 }}> </span>
                  <span style={{ width: 100 }}> </span>
                  <span style={{ width: 160 }}> </span>
                  <span style={{ width: 110 }}> </span>
                  <span style={{ width: 60 }}> </span>
                  <span style={{ width: 80 }}> </span>
                </div>
                <div className="clearfix" />
                <div className="comments-section">
                  <div className="comment-box">
                    <div className="comment-info-head">
                      <div className="comment-user-pic">
                        <a href="#"><img src="img/img17.jpg" /></a>
                      </div>
                      <div className="comment-user-info">
                        <a href="#">
                          Ayşen
                          Gruda
                        </a>
                      </div>
                      <div className="comment-user-rating">
                        <ul>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star-half" /></li>
                          <li><i className="fa fa-star-o" /></li>
                        </ul>
                      </div>
                      <div className="clearfix" />
                    </div>
                    <div className="comment-user-content">
                      <p>
                        it
                        is
                        a
                        long
                        established
                        fact
                        that
                        a
                        reader
                        will
                        be
                        distracted
                        by
                        the
                        readable
                        content
                        of
                        a
                        page
                        when
                        looking
                        at
                        its
                        layout.
                        {" "}
                      </p>
                    </div>
                  </div>
                  <div className="comment-box">
                    <div className="comment-info-head">
                      <div className="comment-user-pic">
                        <a href="#"><img src="img/img17.jpg" /></a>
                      </div>
                      <div className="comment-user-info">
                        <a href="#">
                          Vahi
                          ÖZ
                        </a>
                      </div>
                      <div className="comment-user-rating">
                        <ul>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star-half" /></li>
                          <li><i className="fa fa-star-o" /></li>
                        </ul>
                      </div>
                      <div className="clearfix" />
                    </div>
                    <div className="comment-user-content">
                      <p>
                        it
                        is
                        a
                        long
                        established
                        fact
                        that
                        a
                        reader
                        will
                        be
                        distracted
                        by
                        the
                        readable
                        content
                        of
                        a
                        page
                        when
                        looking
                        at
                        its
                        layout.
                        {" "}
                      </p>
                    </div>
                  </div>
                  <div className="comment-box">
                    <div className="comment-info-head">
                      <div className="comment-user-pic">
                        <a href="#"><img src="img/img17.jpg" /></a>
                      </div>
                      <div className="comment-user-info">
                        <a href="#">
                          Hüseyin
                          Baradan
                        </a>
                      </div>
                      <div className="comment-user-rating">
                        <ul>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star-half" /></li>
                          <li><i className="fa fa-star-o" /></li>
                        </ul>
                      </div>
                      <div className="clearfix" />
                    </div>
                    <div className="comment-user-content">
                      <p>
                        it
                        is
                        a
                        long
                        established
                        fact
                        that
                        a
                        reader
                        will
                        be
                        distracted
                        by
                        the
                        readable
                        content
                        of
                        a
                        page
                        when
                        looking
                        at
                        its
                        layout.
                        {" "}
                      </p>
                    </div>
                  </div>
                  <div className="comment-box">
                    <div className="comment-info-head">
                      <div className="comment-user-pic">
                        <a href="#"><img src="img/img17.jpg" /></a>
                      </div>
                      <div className="comment-user-info">
                        <a href="#">
                          Sadri
                          Alışık
                        </a>
                      </div>
                      <div className="comment-user-rating">
                        <ul>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star-half" /></li>
                          <li><i className="fa fa-star-o" /></li>
                        </ul>
                      </div>
                      <div className="clearfix" />
                    </div>
                    <div className="comment-user-content">
                      <p>
                        it
                        is
                        a
                        long
                        established
                        fact
                        that
                        a
                        reader
                        will
                        be
                        distracted
                        by
                        the
                        readable
                        content
                        of
                        a
                        page
                        when
                        looking
                        at
                        its
                        layout.
                        {" "}
                      </p>
                    </div>
                  </div>
                  <div className="comment-box">
                    <div className="comment-info-head">
                      <div className="comment-user-pic">
                        <a href="#"><img src="img/img17.jpg" /></a>
                      </div>
                      <div className="comment-user-info">
                        <a href="#">
                          Filiz
                          Akın
                        </a>
                      </div>
                      <div className="comment-user-rating">
                        <ul>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star-half" /></li>
                          <li><i className="fa fa-star-o" /></li>
                        </ul>
                      </div>
                      <div className="clearfix" />
                    </div>
                    <div className="comment-user-content">
                      <p>
                        it
                        is
                        a
                        long
                        established
                        fact
                        that
                        a
                        reader
                        will
                        be
                        distracted
                        by
                        the
                        readable
                        content
                        of
                        a
                        page
                        when
                        looking
                        at
                        its
                        layout.
                        {" "}
                      </p>
                    </div>
                  </div>
                  <div className="comment-box">
                    <div className="comment-info-head">
                      <div className="comment-user-pic">
                        <a href="#"><img src="img/img17.jpg" /></a>
                      </div>
                      <div className="comment-user-info">
                        <a href="#">
                          Sami
                          Hazinses
                        </a>
                      </div>
                      <div className="comment-user-rating">
                        <ul>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star-half" /></li>
                          <li><i className="fa fa-star-o" /></li>
                        </ul>
                      </div>
                      <div className="clearfix" />
                    </div>
                    <div className="comment-user-content">
                      <p>
                        it
                        is
                        a
                        long
                        established
                        fact
                        that
                        a
                        reader
                        will
                        be
                        distracted
                        by
                        the
                        readable
                        content
                        of
                        a
                        page
                        when
                        looking
                        at
                        its
                        layout.
                        {" "}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="comment-edit-area">
                  <div className="comment-edit-btn">
                    <a href="#" className="send-classroom-btn">
                      Düzenle
                    </a>
                  </div>
                  <div className="comment-rate-btn">
                    <a href="#" className="send-classroom-btn">
                      Değerlendir
                    </a>
                  </div>
                </div>
                <div className="clearfix" />
              </div>
            </div>
          </div>
          {/* package-player-area */}

          <SearchContentAreaKonteyner />

          {/* search-content-area */}
        </main>
        {/*Footer-Start*/}
        <footer id="footer">
          {/*Footer*/}
          <div className="footer-widget">
            <div className="container">
              <div className="row">
                <div className="col-sm-3">
                  <div className="single-widget">
                    <a href="#">
                      <img
                        src="img/eba-logo.svg"
                        alt="eba-logo"
                        title="Eba-Logo"
                        width={50}
                        height={50}
                      />
                    </a>
                    <h2>
                      Eğitim
                      Bilişim
                      Ağı
                    </h2>
                    <div className="footer-social-icon">
                      <ul>
                        <li>
                          <a href="#">
                            <img src="img/facebook.png" alt="Facebook" />
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <img src="img/twitter.png" alt="Facebook" />
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <img src="img/instagram.png" alt="Facebook" />
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <img src="img/youtube.png" alt="Facebook" />
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="col-sm-2">
                  <div className="single-widget">
                    <h2>Anasayfa</h2>
                    <ul className="footer-widget-list">
                      <li><a href="#">Hakkımızda</a></li>
                      <li><a href="#">Blog</a></li>
                      <li><a href="#">Destek</a></li>
                      <li><a href="#">İletişim</a></li>
                    </ul>
                  </div>
                </div>
                <div className="col-sm-2">
                  <div className="single-widget">
                    <h2>İçerik</h2>
                    <ul className="footer-widget-list">
                      <li><a href="#">Haber</a></li>
                      <li><a href="#">Video</a></li>
                      <li><a href="#">Görsel</a></li>
                      <li><a href="#">Ses</a></li>
                      <li><a href="#">Kitap</a></li>
                      <li><a href="#">Dergi</a></li>
                      <li><a href="#">Döküman</a></li>
                    </ul>
                  </div>
                </div>
                <div className="col-sm-2">
                  <div className="single-widget">
                    <h2>Bağlantılar</h2>
                    <ul className="footer-widget-list">
                      <li>
                        <a href="#">
                          Milli
                          Eğitim
                          Bakanlığı
                        </a>
                      </li>
                      <li><a href="#">Yeğitek</a></li>
                      <li>
                        <a href="#">
                          Fatih
                          Projesi
                        </a>
                      </li>
                      <li><a href="#">UZEM</a></li>
                      <li><a href="#">UES</a></li>
                      <li><a href="#">eTwinning</a></li>
                    </ul>
                  </div>
                </div>
                <div className="col-sm-2">
                  <div className="single-widget">
                    <h2>Araçlar</h2>
                    <ul className="footer-widget-list">
                      <li>
                        <a href="#">
                          İçerik
                          Geliştirme
                          Araçları
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          Yardımcı
                          Programlar
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>
        {/*/Footer-End*/}
        <div id="c-mask" className="c-mask" />
        {/* jQuery */}
        {/* Bootstrap Core JavaScript */}
        {/* Plugins JavaScript */}
        {/* Masonry Javascript */}
        {/* Custom JavaScript */}
      </div>
    );
  }
}
