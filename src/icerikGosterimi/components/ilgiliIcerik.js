import React, { Component } from "react";

export default class IlgiliIcerik extends Component {
  render() {
    var icerik = this.props.icerik;

    return (
      <li className="search-content-results-item masonry-brick">
        <img
          src={icerik.kucukResim}
          alt="İçerik Resimleri"
          title="İçerik Resimleri"
        />
        <p>{icerik.baslik}</p>
        <span className="added-person">
          {icerik.user.fullName}
        </span>
        <span>|</span>
        <span className="added-date">
          {String(icerik.dateCreated)}
        </span>
      </li>
    );
  }
}
