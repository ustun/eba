import React, { Component } from "react";
import $ from "jquery";
import jQueryBridget from "jquery-bridget";
import Masonry from "masonry-layout";
import IlgiliIcerik from "./ilgiliIcerik";
import "./searchContentArea.css";

jQueryBridget("masonry", Masonry, $);

function shouldShowLoadMore(arama) {
  return !arama.fetchInProgress && arama.hasMore;
}

export default class SearchContentArea extends Component {
  componentDidMount() {
    var x = $(this.listeRefi);
    x.masonry({
      itemSelector: ".search-content-results-item",
      isFitWidth: true,
      isAnimated: false
    });

    this.props.fetchData();
  }

  componentDidUpdate() {
    console.log("did update calisti");

    var x = $(this.listeRefi);
    x.masonry("layout");

    setTimeout(function() {
      // console.log('layout in progress')
      x.masonry("layout");
    }, 3000);
  }

  render() {
    const { arama } = this.props;

    return (
      <div>
        <div className="content-search-bar">
          <div className="container">
            <div className>
              <div id="custom-search-input">
                <form onSubmit={this.props.onFormSubmit}>
                  <input
                    onChange={this.props.setMetin}
                    value={arama.metin}
                    type="text"
                    className="form-control content-search-input"
                    placeholder="Ara..."
                  />

                  <button className="btn btn-info btn-lg" type="button">
                    <i className="fa fa-search" />
                  </button>
                </form>
              </div>
            </div>
            <div className="content-search-filter">
              <button className="btn" type="button" title="Filtrele">
                <i className="fa fa-sliders" />
              </button>
            </div>
            <div className="content-search-type">
              <ul className="content-search-type-list">
                <li>
                  <a href="#" className="active">Hepsi</a>
                </li>
                <li><a href="#">Video</a></li>
                <li><a href="#">Etkinlik</a></li>
                <li><a href="#">Resim</a></li>
                <li><a href="#">Döküman</a></li>
              </ul>
            </div>
            <div className="content-search-total-results">
              <span>
                254
                Sonuç<i className="fa fa-sort-desc" />
              </span>
            </div>
            <div className="clearfix" />
          </div>
        </div>
        <div className="search-content-results-list">
          <div className="container">
            <h3 className="foo">
              İlgili
              İçerikler
            </h3>
            <ul
              ref={listeRefi => (this.listeRefi = listeRefi)}
              id="search-content-results-items"
              className="masonry"
              style={{
                position: "relative",
                height: 1108,
                width: 1120
              }}
            >

              {arama.icerikler.map(icerik =>
                <IlgiliIcerik key={icerik.id} icerik={icerik} />
              )}
            </ul>
            <br />

            {this.props.arama.fetchInProgress && <div>Loading</div>}

            {shouldShowLoadMore(this.props.arama) &&
              <button type="button" onClick={this.props.loadMore}>
                Daha
                Fazla
              </button>}
          </div>

        </div>

      </div>
    );
  }
}
