import React, { Component } from "react";

export default class Header extends Component {
  componentDidMount() {
    this.props.getNotifications();
  }

  render() {
    return (
      <header>
        <nav className="navbar custom-navbar eba-header" role="navigation">
          <div className="container">
            <button
              className="navbar-toggle collapsed"
              type="button"
              data-target="#navbar-collapse"
              data-toggle="collapse"
            >
              <span className="sr-only">
                Toggle
                navigation
              </span>
              <span className="icon-bar" />
              <span className="icon-bar" />
              <span className="icon-bar" />
            </button>
            <div className="navbar-header">
              <div className="eba-logo">
                <a className="eba-logo-img" href="/">
                  <img
                    className="svg"
                    src="img/eba-logo-beyaz.svg"
                    onerror="this.onerror=null; this.src='img/eba-logo.png'"
                    alt="Eğitim Bilişim Ağı"
                    width={65}
                    height={43}
                  />
                </a>
              </div>
            </div>
            {/* Non-collapsing right-side icons */}
            <div className="user-tools-area">
              <ul className="list-inline">
                <li className="add-content-btn-area">
                  <a
                    href
                    className="user-tools-link add-content-btn"
                    title="İçerik Ekle"
                  >
                    İçerik
                    Ekle
                  </a>
                </li>
                <li>
                  <a
                    href
                    className="user-tools-link user-icon-link"
                    title="Takvim"
                  >
                    <i className="fa fa-calendar-o" />
                  </a>
                </li>
                <li>
                  <a
                    href
                    className="user-tools-link user-icon-link"
                    title="Mesajlar"
                  >
                    <i className="fa fa-envelope-o" />
                    <span className="notice-badge">
                      {this.props.nUnreadMessages}
                    </span>
                  </a>
                </li>
                <li>
                  <a
                    href
                    className="user-tools-link user-icon-link"
                    title="Bildirimler"
                  >
                    <i className="fa fa-bell-o" />
                    {this.props.notificationsFetched &&
                      this.props.nNotifications !== "0" &&
                      <span className="notice-badge">
                        {this.props.nNotifications}
                      </span>}
                  </a>
                </li>
                <li id="user-dropdown-menu" className="dropdown">
                  <a
                    href="#"
                    className="user-tools-link user-profile-btn dropdown-toggle"
                    data-toggle="dropdown"
                    title="Profil"
                  >
                    <img src={this.props.photoUrl} alt="Profil" />
                  </a>
                  <ul className="dropdown-menu">
                    <li><a href="#">Profilim</a></li>
                    <li><a href="#">Moderasyon</a></li>
                    <li><a href="#">Onaylama</a></li>
                    <li className="divider" />
                    <li><a href="#">Çıkış</a></li>
                  </ul>
                </li>
              </ul>
            </div>
            {/* the collapsing menu */}
            <div
              className="collapse navbar-collapse navbar-left"
              id="navbar-collapse"
            >
              <ul className="nav navbar-nav">
                <li>
                  <a href className="top-menu-link" title="ANASAYFA">
                    ANASAYFA
                  </a>
                </li>
                <li>
                  <a href className="top-menu-link" title="DOSYALARIM">
                    DOSYALARIM
                  </a>
                </li>
                <li>
                  <a href className="top-menu-link" title="KEŞFET">
                    KEŞFET
                  </a>
                </li>
                <li>
                  <a href className="top-menu-link" title="E-KURS">
                    E-KURS
                  </a>
                </li>
                <li>
                  <a href className="top-menu-link" title="YARIŞMALAR">
                    YARIŞMALAR
                  </a>
                </li>
              </ul>
            </div>
            {/*/.nav-collapse */}
          </div>
          {/*/.container */}
        </nav>
      </header>
    );
    {
      /* eba header row*/
    }
  }
}
