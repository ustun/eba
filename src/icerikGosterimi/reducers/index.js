import * as models from "../models/";
import * as constants from "../constants";

export function appReducer(state = models.EbaUygulama2, action) {
  switch (action.type) {
    case constants.SET_N_NOTIFICATIONS:
      return state
        .set("nNotifications", action.payload)
        .set("notificationsFetched", true);
      break;

    case "SET_FETCH_IN_PROGRESS":
      return state.setIn(["arama", "fetchInProgress"], action.payload);
      break;

    case "SET_METIN":
      return state.setIn(["arama", "metin"], action.payload);

    // case 'SET_RELATED_CONTENTS':
    // 	let icerikler = convertIceriklerToImmutable(action.payload);
    // 	// insan: Map({name: 'Ahmet'})
    // 	// insan.name ?
    // 	// insan.get('name')
    // 	// {} []

    // 	return state
    // 	    .setIn(['arama', 'icerikler'], icerikler)
    // 	    .setIn(['arama', 'baslangicIndexi'], icerikler.length)
    // 	    .setIn(['arama', 'fetchComplete'], true);
    // 	break;

    case "RESET_RELATED_CONTENTS":
      return state.set("arama", models.Arama().set("metin", state.arama.metin));

      break;

    case "LOAD_MORE_RELATED_CONTENTS":
      // return state.setIn(['arama', 'fetchComplete'], true);

      const icerikler = models.convertIceriklerToImmutable(action.payload);
      const eskiIcerikler = state.getIn(["arama", "icerikler"]);
      const tumIcerikler = eskiIcerikler.concat(icerikler);

      return state
        .setIn(["arama", "icerikler"], tumIcerikler)
        .setIn(["arama", "baslangicIndexi"], tumIcerikler.count())
        .setIn(["arama", "fetchComplete"], true)
        .setIn(["arama", "hasMore"], icerikler.count() > 0);
      break;
  }

  return state;
}
