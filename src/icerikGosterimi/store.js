import { applyMiddleware, createStore } from "redux";
import { appReducer } from "./reducers";
import { createLogger } from "redux-logger";
import { Iterable } from "immutable";

const stateTransformer = state => {
  if (Iterable.isIterable(state)) return state.toJS();
  else return state;
};

const logger = createLogger({
  stateTransformer
});

const store = createStore(appReducer, applyMiddleware(logger));

if (process.env.NODE_ENV == "development") {
  window.store = store;
}

// window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
export default store;

// store.subscribe(() => {
//     console.log(
// 	"icerikler",
// 	store.getState().toJS(),
// 	store.getState().arama.toJS(),
// 	store.getState().getIn(["arama", "icerikler"])
//     );
// });
