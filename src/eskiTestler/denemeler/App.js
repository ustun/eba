import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { createStore } from "redux";

export function sayaciArtir(sayaclar, artirilacakId) {
  var yeniSayaclar = sayaclar.map(sayac => {
    if (artirilacakId === sayac.id) {
      return {
        id: sayac.id,
        value: sayac.value + 1
      };
    } else {
      return {
        id: sayac.id,
        value: sayac.value - 1
      };
    }
  });
  return yeniSayaclar;
}

class MerhabaDunya extends React.Component {
  karakterSayisi() {
    return this.props.insan.isim.length;
  }

  render() {
    var { yas, isim } = this.props.insan;

    return (
      <div>
        Hello
        {" "}
        {isim}.
        Sizin
        isminiz
        {" "}
        {this.karakterSayisi()}
        {" "}harf
        Benim
        yasim
        {" "}
        {yas}
      </div>
    );
  }
}

MerhabaDunya.defaultProps = {
  insan: {
    yas: 11,
    isim: ""
  }
};

var yas = 32;
var isim = "ali";

class Selamlama extends Component {
  render() {
    var insanlar = [
      {
        isim: "Ali",
        yas: 24
      },
      {
        isim: "Ayse",
        yas: 25
      },
      {
        isim: "Mehmet",
        yas: 26
      }
    ];

    return (
      <div>
        {insanlar
          .filter(insan => insan.isim.startsWith("A"))
          .map(insan => <MerhabaDunya insan={insan} />)}
      </div>
    );
  }
}

class SayacView extends Component {
  render() {
    // sayac = {id:1 , value: 4}
    return (
      <div>
        sayac
        {" "}
        {this.props.sayac.value}
        <button onClick={this.props.artir}>
          Artir
        </button>
      </div>
    );
  }
}

class Sayaclar extends Component {
  state = {
    sayaclar: [
      {
        id: 1,
        value: 3
      },
      {
        id: 2,
        value: 6
      },
      {
        id: 3,
        value: 4
      },
      {
        id: 4,
        value: 7
      }
    ]
  };

  sayaciArtir = tiklananSayacId => {
    this.setState({
      sayaclar: sayaciArtir(this.state.sayaclar, tiklananSayacId)
    });
  };

  // https://gitlab.com/ustun/eba

  render() {
    return (
      <div>
        {this.state.sayaclar.map(sayac =>
          <SayacView artir={() => this.sayaciArtir(sayac.id)} sayac={sayac} />
        )}
      </div>
    );
  }
}

class TodoComponent extends Component {
  render() {
    return (
      <li className={this.props.item.completed && "completed"}>
        <div className="view">
          <input
            onClick={() => this.props.toggleTodo(this.props.item.tanim)}
            checked={this.props.item.completed}
            className="toggle"
            type="checkbox"
          />
          <label>{this.props.item.tanim}</label>
          <button
            onClick={() => this.props.destroyTodo(this.props.item.tanim)}
            className="destroy"
          />
        </div>
      </li>
    );
  }
}

class TodoApp extends Component {
  state = {
    criteria: "All",
    tempText: "",
    todos: [
      toggleTodo(newTodo("a")),
      toggleTodo(newTodo("b")),
      newTodo("sdfsdf"),
      newTodo("d")
    ]
  };

  setCriteria(newCriteria) {
    this.setState({
      criteria: newCriteria
    });
  }

  toggleTodo = tanim => {
    var newTodos = toggleTodoInList(this.state.todos, tanim);

    this.setState({
      todos: newTodos
    });
  };

  destroyTodo = tanim => {
    var newTodos = removeTodoFromList(this.state.todos, tanim);

    this.setState({
      todos: newTodos
    });
  };

  handleTempTextChange = event => {
    this.setState({
      tempText: event.target.value
    });
  };

  formSubmit = event => {
    event.preventDefault();

    this.setState({
      tempText: "",
      todos: addNewTodo(this.state.todos, newTodo(this.state.tempText))
    });
  };

  clearCompleted = () => {
    this.setState({
      todos: clearCompletedTodos(this.state.todos)
    });
  };

  render() {
    var todos = filterTodos(this.state.todos, this.state.criteria);

    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <div>{this.state.tempText}</div>
          <form onSubmit={this.formSubmit}>
            <input
              value={this.state.tempText}
              onChange={this.handleTempTextChange}
              className="new-todo"
              placeholder="What needs to be done?"
              autofocus
            />
          </form>
        </header>
        <section
          className="main"
          style={{
            display: "block"
          }}
        >
          <input className="toggle-all" type="checkbox" />
          <label htmlFor="toggle-all">
            Mark
            all
            as
            complete
          </label>
          <ul className="todo-list">
            {todos.map(item =>
              <TodoComponent
                toggleTodo={this.toggleTodo}
                destroyTodo={this.destroyTodo}
                item={item}
              />
            )}
          </ul>
        </section>
        <footer
          className="footer"
          style={{
            display: "block"
          }}
        >
          <span className="todo-count">
            <strong>{nLeftTodos(this.state.todos)}</strong>
            {" "}items
            left
          </span>
          <ul className="filters">
            <li>
              <a
                onClick={() => this.setCriteria("All")}
                href="#/"
                className={this.state.criteria === "All" && "selected"}
              >
                All
              </a>
            </li>
            <li>
              <a
                onClick={() => this.setCriteria("Active")}
                href="#/active"
                className={this.state.criteria === "Active" && "selected"}
              >
                Active
              </a>
            </li>
            <li>
              <a
                onClick={() => this.setCriteria("Completed")}
                href="#/completed"
                className={this.state.criteria === "Completed" && "selected"}
              >
                Completed
              </a>
            </li>
          </ul>
          <button
            onClick={this.clearCompleted}
            className="clear-completed"
            style={{
              display: "block"
            }}
          >
            Clear
            completed
          </button>
        </footer>
      </section>
    );
  }
}

export function newTodo(tanim) {
  return {
    tanim,
    completed: false
  };
}

export function addNewTodo(todos, todo) {
  return [...todos, todo];
}

export function toggleTodo(todo) {
  var newTodo = {
    ...todo,
    completed: !todo.completed
  };

  return newTodo;
}

export function toggleTodoInList(oldTodos, targetTodoTanim) {
  var newTodos = oldTodos.map(todo => {
    if (todo.tanim === targetTodoTanim) {
      return toggleTodo(todo);
    } else {
      return todo;
    }
  });

  return newTodos;
}

export function nLeftTodos(todos) {
  return todos.filter(todo => !todo.completed).length;
}

export function filterTodos(todos, criteria) {
  if (criteria == "All") {
    return todos;
  }

  if (criteria == "Completed") {
    return todos.filter(todo => todo.completed);
  }

  if (criteria == "Active") {
    return todos.filter(todo => !todo.completed);
  }
}

export function removeTodoFromList(todos, tanim) {
  return todos.filter(todo => todo.tanim != tanim);
}

export function clearCompletedTodos(todos) {
  return todos.filter(todo => !todo.completed);
}

class CocukBilesen extends Component {
  render() {
    return (
      <span>
        Ben
        cocugum.
        {" "}
        {this.props.isim}
      </span>
    );
  }
}

class AnneBilesen extends Component {
  render() {
    var gosterilecekCocuklar = React.Children
      .toArray(this.props.children)
      .filter((item, index) => index % 2 === 0);

    return (
      <div>
        Ben
        anneyim.
        Ismin
        {" "}
        {this.props.isim}
        {gosterilecekCocuklar}
      </div>
    );
  }
}

class MerhabaDunya2 extends Component {
  render() {
    return (
      <AnneBilesen isim="Reyhan">
        <CocukBilesen isim="Ali" />
        <CocukBilesen isim="Ali2" />
        <CocukBilesen isim="Ali3" />
      </AnneBilesen>
    );
  }
}

// export default MerhabaDunya2

// export default TodoApp;

// .x { color : red;}

class RefOrnegi extends Component {
  componentWillMount() {
    console.log("will mount", document.querySelector(".x"));
  }

  componentDidMount() {
    console.log("did mount with my ref", this.myref);
  }

  render() {
    return (
      <div>

        <div className="x" ref={gercekDom => (this.myref = gercekDom)}>
          Merhaba
          Dunya
        </div>

        <span>
          Burasi
          kirmizi
        </span>

      </div>
    );
  }
}

// export default RefOrnegi;

class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sayac: this.props.ilkSayac
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.ilkSayac !== this.props.ilkSayac) {
      this.setState({
        sayac: nextProps.ilkSayac
      });
    }
    console.log("componentWillReceiveProps", arguments);
  }

  componentDidUpdate() {
    console.log("did update", arguments);
  }

  componentWillUnmount() {
    console.log("will unmount", arguments);
  }

  componentDidMount() {
    console.log("did mount", arguments);
  }

  render() {
    return (
      <div>
        {this.state.sayac}
        <button
          onClick={() =>
            this.setState({
              sayac: this.state.sayac + 1
            })}
        >
          Artir
        </button>
      </div>
    );
  }
}

Counter.defaultProps = { ilkSayac: 0 };

class KeyOrnegi extends Component {
  state = {
    sayac: 10
  };

  render() {
    var insanlar = [
      {
        isim: "ali",
        id: 1
      },
      {
        isim: "ali",
        id: 2
      },
      {
        isim: "ali",
        id: 3
      }
    ];

    return (
      <div>

        Ana
        bilesen
        {" "}
        {this.state.sayac}
        <button
          onClick={() =>
            this.setState({
              sayac: this.state.sayac + 1
            })}
        >
          Artir
        </button>

        <ul>
          {insanlar.map(insan =>
            <Counter
              ilkSayac={this.state.sayac}
              key={/* this.state.sayac + '_' + */ insan.id}
            >
              {insan.isim}
            </Counter>
          )}
        </ul>
      </div>
    );
  }
}

// export default KeyOrnegi

export function sayacReducer(state = { count: 0 }, action) {
  var newState = state;

  switch (action.type) {
    case "ARTIR":
      newState = {
        count: state.count + 1
      };
      break;
    case "AZALT":
      newState = {
        count: state.count - 1
      };
      break;
    case "RESET":
      newState = {
        count: 0
      };
      break;
  }

  return newState;
}

var store = createStore(sayacReducer);

var propVerisiGeneratoru = function(state) {
  return {
    count: state.count
  };
};

var propFonksiyonuGeneratoru = function(dispatch) {
  return {
    artir: () =>
      dispatch({
        type: "ARTIR"
      }),
    azalt: () =>
      dispatch({
        type: "AZALT"
      }),
    reset: () =>
      dispatch({
        type: "RESET"
      })
  };
};

class SafSayac extends Component {
  render() {
    return (
      <div>
        Sayacin
        degeri:
        {" "}
        {this.props.count}
        <button onClick={this.props.artir}>
          Artir
        </button>

        <button onClick={this.props.azalt}>
          Azalt
        </button>

        <button onClick={this.props.reset}>
          Reset
        </button>
      </div>
    );
  }
}

function connect(
  store,
  statetenVeriPropuUretenFonksiyon,
  dispatchtenFonksiyonPropuUretenFonksiyon,
  SafBilesen
) {
  return class SayacKonteyneri extends Component {
    constructor() {
      super();

      store.subscribe(() => {
        var state = store.getState();
        this.setState(statetenVeriPropuUretenFonksiyon(state));
      });

      this.state = statetenVeriPropuUretenFonksiyon(store.getState());
    }

    render() {
      // react-redux

      var veriProplari = this.state;

      var fonksiyonProplari = dispatchtenFonksiyonPropuUretenFonksiyon(
        store.dispatch
      );

      return <SafBilesen {...veriProplari} {...fonksiyonProplari} />;
    }
  };
}

var MyClass = connect(
  store,
  propVerisiGeneratoru,
  propFonksiyonuGeneratoru,
  SafSayac
);

export default MyClass;
