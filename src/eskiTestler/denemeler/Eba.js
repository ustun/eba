import React, { Component } from "react";
import { connect, Provider } from "react-redux";
import $ from "jquery";
// import 'masonry-layout';
import jQueryBridget from "jquery-bridget";
import Masonry from "masonry-layout";
import { store, getNotificationsString, Api } from "./Ebabusiness";
// make Masonry a jQuery plugin

jQueryBridget("masonry", Masonry, $);

// import moment from 'moment';

class IlgiliIcerik extends Component {
  render() {
    var icerik = this.props.icerik;

    return (
      <li className="search-content-results-item masonry-brick">
        <img
          src={icerik.kucukResim}
          alt="İçerik Resimleri"
          title="İçerik Resimleri"
        />
        <p>{icerik.baslik}</p>
        <span className="added-person">
          {icerik.user.fullName}
        </span>
        <span>|</span>
        <span className="added-date">
          {String(icerik.dateCreated)}
        </span>
      </li>
    );
  }
}

function shouldShowLoadMore(arama) {
  return !arama.fetchInProgress && arama.hasMore;
}

class SearchContentArea extends Component {
  componentDidMount() {
    var x = $(this.listeRefi);
    x.masonry({
      itemSelector: ".search-content-results-item",
      isFitWidth: true,
      isAnimated: false
    });

    this.props.fetchData();
  }

  componentDidUpdate() {
    console.log("did update calisti");

    var x = $(this.listeRefi);
    x.masonry("layout");

    setTimeout(function() {
      // console.log('layout in progress')
      x.masonry("layout");
    }, 3000);
  }

  render() {
    const { arama } = this.props;

    return (
      <div>
        <div className="content-search-bar">
          <div className="container">
            <div className>
              <div id="custom-search-input">
                <form onSubmit={this.props.onFormSubmit}>
                  <input
                    onChange={this.props.setMetin}
                    value={arama.metin}
                    type="text"
                    className="form-control content-search-input"
                    placeholder="Ara..."
                  />

                  <button className="btn btn-info btn-lg" type="button">
                    <i className="fa fa-search" />
                  </button>
                </form>
              </div>
            </div>
            <div className="content-search-filter">
              <button className="btn" type="button" title="Filtrele">
                <i className="fa fa-sliders" />
              </button>
            </div>
            <div className="content-search-type">
              <ul className="content-search-type-list">
                <li>
                  <a href="#" className="active">Hepsi</a>
                </li>
                <li><a href="#">Video</a></li>
                <li><a href="#">Etkinlik</a></li>
                <li><a href="#">Resim</a></li>
                <li><a href="#">Döküman</a></li>
              </ul>
            </div>
            <div className="content-search-total-results">
              <span>
                254
                Sonuç<i className="fa fa-sort-desc" />
              </span>
            </div>
            <div className="clearfix" />
          </div>
        </div>
        <div className="search-content-results-list">
          <div className="container">
            <h3>
              İlgili
              İçerikler
            </h3>
            <ul
              ref={listeRefi => (this.listeRefi = listeRefi)}
              id="search-content-results-items"
              className="masonry"
              style={{
                position: "relative",
                height: 1108,
                width: 1120
              }}
            >

              {arama.icerikler.map(icerik =>
                <IlgiliIcerik key={icerik.id} icerik={icerik} />
              )}
            </ul>
            <br />

            {this.props.arama.fetchInProgress && <div>Loading</div>}

            {shouldShowLoadMore(this.props.arama) &&
              <button type="button" onClick={this.props.loadMore}>
                Daha
                Fazla
              </button>}
          </div>

        </div>

      </div>
    );
  }
}

function mapStateToPropsSearch(appState) {
  return {
    arama: appState.arama
  };
}

function mapDispatchToPropsSearch(dispatch, getState) {
  var functions = {
    loadMore() {
      functions.fetchData(
        store.getState().getIn(["arama", "metin"]),
        store.getState().getIn(["arama", "baslangicIndexi"])
      );
    },

    onFormSubmit(event) {
      event.preventDefault();

      dispatch({
        type: "RESET_RELATED_CONTENTS"
      });

      functions.loadMore();
    },

    setMetin(event) {
      dispatch({
        type: "SET_METIN",
        payload: event.target.value
      });
    },

    async fetchData(metin, baslangicIndexi) {
      if (store.getState().arama.fetchInProgress) {
        return;
      }

      var response;
      dispatch({
        type: "SET_FETCH_IN_PROGRESS",
        payload: true
      });
      try {
        response = await Api.getRelatedContents(metin, baslangicIndexi);
      } catch (e) {
        console.error(e);
      }

      if (response) {
        if (response.status == "200") {
          dispatch({
            type: "LOAD_MORE_RELATED_CONTENTS",
            payload: response.data.data
          });
        } else {
          // save failure
        }
      }

      dispatch({
        type: "SET_FETCH_IN_PROGRESS",
        payload: false
      });
    }
  };

  return functions;
}

const SearchContentAreaKonteyner = connect(
  mapStateToPropsSearch,
  mapDispatchToPropsSearch
)(SearchContentArea);

class EbaPure extends Component {
  render() {
    return (
      <div>
        <header>
          <nav className="navbar custom-navbar eba-header" role="navigation">
            <div className="container">
              <button
                className="navbar-toggle collapsed"
                type="button"
                data-target="#navbar-collapse"
                data-toggle="collapse"
              >
                <span className="sr-only">
                  Toggle
                  navigation
                </span>
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </button>
              <div className="navbar-header">
                <div className="eba-logo">
                  <a className="eba-logo-img" href="/">
                    <img
                      className="svg"
                      src="img/eba-logo-beyaz.svg"
                      onerror="this.onerror=null; this.src='img/eba-logo.png'"
                      alt="Eğitim Bilişim Ağı"
                      width={65}
                      height={43}
                    />
                  </a>
                </div>
              </div>
              {/* Non-collapsing right-side icons */}
              <div className="user-tools-area">
                <ul className="list-inline">
                  <li className="add-content-btn-area">
                    <a
                      href
                      className="user-tools-link add-content-btn"
                      title="İçerik Ekle"
                    >
                      İçerik
                      Ekle
                    </a>
                  </li>
                  <li>
                    <a
                      href
                      className="user-tools-link user-icon-link"
                      title="Takvim"
                    >
                      <i className="fa fa-calendar-o" />
                    </a>
                  </li>
                  <li>
                    <a
                      href
                      className="user-tools-link user-icon-link"
                      title="Mesajlar"
                    >
                      <i className="fa fa-envelope-o" />
                      <span className="notice-badge">
                        {this.props.nUnreadMessages}
                      </span>
                    </a>
                  </li>
                  <li>
                    <a
                      href
                      className="user-tools-link user-icon-link"
                      title="Bildirimler"
                    >
                      <i className="fa fa-bell-o" />
                      {this.props.notificationsFetched &&
                        this.props.nNotifications !== "0" &&
                        <span className="notice-badge">
                          {this.props.nNotifications}
                        </span>}
                    </a>
                  </li>
                  <li id="user-dropdown-menu" className="dropdown">
                    <a
                      href="#"
                      className="user-tools-link user-profile-btn dropdown-toggle"
                      data-toggle="dropdown"
                      title="Profil"
                    >
                      <img src={this.props.photoUrl} alt="Profil" />
                    </a>
                    <ul className="dropdown-menu">
                      <li><a href="#">Profilim</a></li>
                      <li><a href="#">Moderasyon</a></li>
                      <li><a href="#">Onaylama</a></li>
                      <li className="divider" />
                      <li><a href="#">Çıkış</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
              {/* the collapsing menu */}
              <div
                className="collapse navbar-collapse navbar-left"
                id="navbar-collapse"
              >
                <ul className="nav navbar-nav">
                  <li>
                    <a href className="top-menu-link" title="ANASAYFA">
                      ANASAYFA
                    </a>
                  </li>
                  <li>
                    <a href className="top-menu-link" title="DOSYALARIM">
                      DOSYALARIM
                    </a>
                  </li>
                  <li>
                    <a href className="top-menu-link" title="KEŞFET">
                      KEŞFET
                    </a>
                  </li>
                  <li>
                    <a href className="top-menu-link" title="E-KURS">
                      E-KURS
                    </a>
                  </li>
                  <li>
                    <a href className="top-menu-link" title="YARIŞMALAR">
                      YARIŞMALAR
                    </a>
                  </li>
                </ul>
              </div>
              {/*/.nav-collapse */}
            </div>
            {/*/.container */}
          </nav>
        </header>
        {/* eba header row*/}
        <main>
          <div className="lesson-bar">
            <div className="container">
              <div className="date-and-package-list">
                <div className="lesson-date">
                  <div className="current-day-number">17</div>
                  <div className="current-day-other">
                    <div className="current-month-year">
                      Nisan
                      2017
                    </div>
                    <div className="current-day-name">
                      Perşembe
                    </div>
                  </div>
                </div>
                <div className="lesson-package-list-area">
                  <ul className="list-inline lesson-package-list">
                    <li className="active-package">
                      <a
                        href="#"
                        className="lesson-package-link"
                        title="Paket 1"
                      >
                        <span className="lesson-package-name">
                          Paket
                          1
                        </span>
                        <div className="lesson-package-thumb">
                          <img
                            src="http://placehold.it/60x40/cccc00/ffffff?"
                            alt="Paket 1"
                          />
                        </div>
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="lesson-package-link"
                        title="Paket 2"
                      >
                        <span className="lesson-package-name">
                          Paket
                          2
                        </span>
                        <div className="lesson-package-thumb">
                          <img
                            src="http://placehold.it/60x40/ff6600/ffffff"
                            alt="Paket 2"
                          />
                        </div>
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="lesson-package-link"
                        title="Paket 3"
                      >
                        <span className="lesson-package-name">
                          Paket
                          3
                        </span>
                        <div className="lesson-package-thumb">
                          <img
                            src="http://placehold.it/60x40/66ccff/ffffff"
                            alt="Paket 3"
                          />
                        </div>
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        className="lesson-package-link"
                        title="Paket 4"
                      >
                        <span className="lesson-package-name">
                          Paket
                          4
                        </span>
                        <div className="lesson-package-thumb">
                          <img
                            src="http://placehold.it/60x40/919191/ffffff"
                            alt="Paket 4"
                          />
                        </div>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="lesson-code-and-menu-area">
                <div className="lesson-code-title">
                  Derse
                  bağlanmak
                  için:
                </div>
                <div className="lesson-code">45a7</div>
                <div id="o-wrapper" className="o-wrapper classroom-menu">
                  <a id="c-button--slide-right" className="classroom-menu-link">
                    <i className="fa fa-list" />
                  </a>
                </div>
                <nav
                  id="c-menu--slide-right"
                  className="c-menu c-menu--slide-right"
                >
                  <button className="c-menu__close">
                    <i className="fa fa-close" />Kapat
                    →
                  </button>
                  <ul className="c-menu__items">
                    <li>
                      <a href="#" className>
                        Liste
                        1
                      </a>
                    </li>
                    <li>
                      <a href="#" className>
                        Liste
                        2
                      </a>
                    </li>
                    <li>
                      <a href="#" className>
                        Liste
                        3
                      </a>
                    </li>
                    <li>
                      <a href="#" className>
                        Liste
                        4
                      </a>
                    </li>
                    <li>
                      <a href="#" className>
                        Liste
                        5
                      </a>
                    </li>
                  </ul>
                </nav>
                <div className="clearfix" />
              </div>
              <div className="clearfix" />
            </div>
          </div>
          {/* lesson-bar */}
          <div className="package-player-area full-screen-player">
            <div className="container">
              <div className="package-slide-dots">
                <ul>
                  <li>
                    <a href="#" />
                  </li>
                  <li>
                    <a href="#" />
                  </li>
                  <li>
                    <a href="#" />
                  </li>
                  <li>
                    <a href="#" />
                  </li>
                  <li>
                    <a href="#" />
                  </li>
                </ul>
              </div>
              <div className="package-content-list-area">
                <h3 className="package-content-title">
                  Geniş
                  Açı
                </h3>
                <div className="package-content-list">
                  <ul className="list-unstyled">
                    <li className="package-content-item active-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/FFC107/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          Lorem
                          Ipsum
                          Dolor
                          Sit
                          Amet
                        </h4>
                        <span className="package-content-item-type">
                          Konu
                          Anlatımı
                        </span>
                      </a>
                    </li>
                    <li className="package-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/ba68c8/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          İçerik
                          Başlığı
                        </h4>
                        <span className="package-content-item-type">
                          Etkileşim
                        </span>
                      </a>
                    </li>
                    <li className="package-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/9ccc65/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          İçerik
                        </h4>
                        <span className="package-content-item-type">
                          Simülasyon
                        </span>
                      </a>
                    </li>
                    <li className="package-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/4dd0e1/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          Konu
                          Değerlendirmesi
                        </h4>
                        <span className="package-content-item-type">
                          Sınav
                        </span>
                      </a>
                    </li>
                    <li className="package-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/ba68c8/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          İçerik
                          Başlığı
                        </h4>
                        <span className="package-content-item-type">
                          Etkileşim
                        </span>
                      </a>
                    </li>
                    <li className="package-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/9ccc65/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          İçerik
                        </h4>
                        <span className="package-content-item-type">
                          Simülasyon
                        </span>
                      </a>
                    </li>
                    <li className="package-content-item">
                      <a href="#" className="package-content-item-link">
                        <span className="package-content-item-thumb">
                          <img
                            src="http://placehold.it/60x40/4dd0e1/ffffff"
                            alt
                          />
                        </span>
                        <h4 className="package-content-item-title">
                          Konu
                          Değerlendirmesi
                        </h4>
                        <span className="package-content-item-type">
                          Sınav
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="package-content-footer">
                  <a href="#" className="send-classroom-btn">
                    Sınıfa
                    Gönder
                  </a>
                </div>
              </div>
              <div className="package-player full-width-package-player">
                <img
                  src="img/content/video-player.jpg"
                  alt="Player"
                  width={1140}
                  height={600}
                />
              </div>
              <div className="package-player-controls">
                <div className="package-player-prev-btn">
                  <a href="#" className="prev arrows">
                    <i className="fa fa-arrow-left" />
                  </a>
                </div>
                <div className="package-player-next-btn">
                  <a href="#" className="next arrows">
                    <i className="fa fa-arrow-right" />
                  </a>
                </div>
              </div>
              <div className="clearfix" />
              <div className="package-content-detail">
                <div className="package-content-detail-topic">
                  <span>İçerik1</span>
                  <span className="seperator">|</span>
                  <span>
                    Ahmet
                    Beyaz
                  </span>
                </div>
                <div className="package-content-detail-button">
                  <button
                    id="detail-toggle-btn"
                    type="button"
                    title="Ayrıntılar"
                  >
                    <i className="fa fa-info" />
                  </button>
                  <span>
                    <i className="fa fa-sort-desc" />
                  </span>
                  <span>
                    <i className="fa fa-sort-asc" style={{ display: "none" }} />
                  </span>
                </div>
                <div className="package-content-send-btn">
                  <a href="#" className="send-classroom-btn">
                    Sınıfa
                    Gönder
                  </a>
                </div>
              </div>
              <div className="clearfix" />
              <div className="package-content-comments">
                <div className="package-rating">
                  <div className="package-rating-teach">
                    <ul>
                      <li><i className="fa fa-star" /></li>
                      <li><i className="fa fa-star" /></li>
                      <li><i className="fa fa-star" /></li>
                      <li><i className="fa fa-star-half" /></li>
                      <li><i className="fa fa-star-o" /></li>
                    </ul>
                    <span>
                      12
                      Öğretmen
                    </span>
                  </div>
                  <div className="package-rating-student">
                    <ul>
                      <li><i className="fa fa-star" /></li>
                      <li><i className="fa fa-star" /></li>
                      <li><i className="fa fa-star" /></li>
                      <li><i className="fa fa-star-half" /></li>
                      <li><i className="fa fa-star-o" /></li>
                    </ul>
                    <span>
                      75
                      Öğrenci
                    </span>
                  </div>
                </div>
                <div className="package-rating-bars">
                  <span style={{ width: 200 }}> </span>
                  <span style={{ width: 180 }}> </span>
                  <span style={{ width: 150 }}> </span>
                  <span style={{ width: 120 }}> </span>
                  <span style={{ width: 170 }}> </span>
                  <span style={{ width: 100 }}> </span>
                  <span style={{ width: 160 }}> </span>
                  <span style={{ width: 110 }}> </span>
                  <span style={{ width: 60 }}> </span>
                  <span style={{ width: 80 }}> </span>
                </div>
                <div className="clearfix" />
                <div className="comments-section">
                  <div className="comment-box">
                    <div className="comment-info-head">
                      <div className="comment-user-pic">
                        <a href="#"><img src="img/img17.jpg" /></a>
                      </div>
                      <div className="comment-user-info">
                        <a href="#">
                          Ayşen
                          Gruda
                        </a>
                      </div>
                      <div className="comment-user-rating">
                        <ul>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star-half" /></li>
                          <li><i className="fa fa-star-o" /></li>
                        </ul>
                      </div>
                      <div className="clearfix" />
                    </div>
                    <div className="comment-user-content">
                      <p>
                        it
                        is
                        a
                        long
                        established
                        fact
                        that
                        a
                        reader
                        will
                        be
                        distracted
                        by
                        the
                        readable
                        content
                        of
                        a
                        page
                        when
                        looking
                        at
                        its
                        layout.
                        {" "}
                      </p>
                    </div>
                  </div>
                  <div className="comment-box">
                    <div className="comment-info-head">
                      <div className="comment-user-pic">
                        <a href="#"><img src="img/img17.jpg" /></a>
                      </div>
                      <div className="comment-user-info">
                        <a href="#">
                          Vahi
                          ÖZ
                        </a>
                      </div>
                      <div className="comment-user-rating">
                        <ul>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star-half" /></li>
                          <li><i className="fa fa-star-o" /></li>
                        </ul>
                      </div>
                      <div className="clearfix" />
                    </div>
                    <div className="comment-user-content">
                      <p>
                        it
                        is
                        a
                        long
                        established
                        fact
                        that
                        a
                        reader
                        will
                        be
                        distracted
                        by
                        the
                        readable
                        content
                        of
                        a
                        page
                        when
                        looking
                        at
                        its
                        layout.
                        {" "}
                      </p>
                    </div>
                  </div>
                  <div className="comment-box">
                    <div className="comment-info-head">
                      <div className="comment-user-pic">
                        <a href="#"><img src="img/img17.jpg" /></a>
                      </div>
                      <div className="comment-user-info">
                        <a href="#">
                          Hüseyin
                          Baradan
                        </a>
                      </div>
                      <div className="comment-user-rating">
                        <ul>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star-half" /></li>
                          <li><i className="fa fa-star-o" /></li>
                        </ul>
                      </div>
                      <div className="clearfix" />
                    </div>
                    <div className="comment-user-content">
                      <p>
                        it
                        is
                        a
                        long
                        established
                        fact
                        that
                        a
                        reader
                        will
                        be
                        distracted
                        by
                        the
                        readable
                        content
                        of
                        a
                        page
                        when
                        looking
                        at
                        its
                        layout.
                        {" "}
                      </p>
                    </div>
                  </div>
                  <div className="comment-box">
                    <div className="comment-info-head">
                      <div className="comment-user-pic">
                        <a href="#"><img src="img/img17.jpg" /></a>
                      </div>
                      <div className="comment-user-info">
                        <a href="#">
                          Sadri
                          Alışık
                        </a>
                      </div>
                      <div className="comment-user-rating">
                        <ul>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star-half" /></li>
                          <li><i className="fa fa-star-o" /></li>
                        </ul>
                      </div>
                      <div className="clearfix" />
                    </div>
                    <div className="comment-user-content">
                      <p>
                        it
                        is
                        a
                        long
                        established
                        fact
                        that
                        a
                        reader
                        will
                        be
                        distracted
                        by
                        the
                        readable
                        content
                        of
                        a
                        page
                        when
                        looking
                        at
                        its
                        layout.
                        {" "}
                      </p>
                    </div>
                  </div>
                  <div className="comment-box">
                    <div className="comment-info-head">
                      <div className="comment-user-pic">
                        <a href="#"><img src="img/img17.jpg" /></a>
                      </div>
                      <div className="comment-user-info">
                        <a href="#">
                          Filiz
                          Akın
                        </a>
                      </div>
                      <div className="comment-user-rating">
                        <ul>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star-half" /></li>
                          <li><i className="fa fa-star-o" /></li>
                        </ul>
                      </div>
                      <div className="clearfix" />
                    </div>
                    <div className="comment-user-content">
                      <p>
                        it
                        is
                        a
                        long
                        established
                        fact
                        that
                        a
                        reader
                        will
                        be
                        distracted
                        by
                        the
                        readable
                        content
                        of
                        a
                        page
                        when
                        looking
                        at
                        its
                        layout.
                        {" "}
                      </p>
                    </div>
                  </div>
                  <div className="comment-box">
                    <div className="comment-info-head">
                      <div className="comment-user-pic">
                        <a href="#"><img src="img/img17.jpg" /></a>
                      </div>
                      <div className="comment-user-info">
                        <a href="#">
                          Sami
                          Hazinses
                        </a>
                      </div>
                      <div className="comment-user-rating">
                        <ul>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star" /></li>
                          <li><i className="fa fa-star-half" /></li>
                          <li><i className="fa fa-star-o" /></li>
                        </ul>
                      </div>
                      <div className="clearfix" />
                    </div>
                    <div className="comment-user-content">
                      <p>
                        it
                        is
                        a
                        long
                        established
                        fact
                        that
                        a
                        reader
                        will
                        be
                        distracted
                        by
                        the
                        readable
                        content
                        of
                        a
                        page
                        when
                        looking
                        at
                        its
                        layout.
                        {" "}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="comment-edit-area">
                  <div className="comment-edit-btn">
                    <a href="#" className="send-classroom-btn">
                      Düzenle
                    </a>
                  </div>
                  <div className="comment-rate-btn">
                    <a href="#" className="send-classroom-btn">
                      Değerlendir
                    </a>
                  </div>
                </div>
                <div className="clearfix" />
              </div>
            </div>
          </div>
          {/* package-player-area */}

          <SearchContentAreaKonteyner />

          {/* search-content-area */}
        </main>
        {/*Footer-Start*/}
        <footer id="footer">
          {/*Footer*/}
          <div className="footer-widget">
            <div className="container">
              <div className="row">
                <div className="col-sm-3">
                  <div className="single-widget">
                    <a href="#">
                      <img
                        src="img/eba-logo.svg"
                        alt="eba-logo"
                        title="Eba-Logo"
                        width={50}
                        height={50}
                      />
                    </a>
                    <h2>
                      Eğitim
                      Bilişim
                      Ağı
                    </h2>
                    <div className="footer-social-icon">
                      <ul>
                        <li>
                          <a href="#">
                            <img src="img/facebook.png" alt="Facebook" />
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <img src="img/twitter.png" alt="Facebook" />
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <img src="img/instagram.png" alt="Facebook" />
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <img src="img/youtube.png" alt="Facebook" />
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="col-sm-2">
                  <div className="single-widget">
                    <h2>Anasayfa</h2>
                    <ul className="footer-widget-list">
                      <li><a href="#">Hakkımızda</a></li>
                      <li><a href="#">Blog</a></li>
                      <li><a href="#">Destek</a></li>
                      <li><a href="#">İletişim</a></li>
                    </ul>
                  </div>
                </div>
                <div className="col-sm-2">
                  <div className="single-widget">
                    <h2>İçerik</h2>
                    <ul className="footer-widget-list">
                      <li><a href="#">Haber</a></li>
                      <li><a href="#">Video</a></li>
                      <li><a href="#">Görsel</a></li>
                      <li><a href="#">Ses</a></li>
                      <li><a href="#">Kitap</a></li>
                      <li><a href="#">Dergi</a></li>
                      <li><a href="#">Döküman</a></li>
                    </ul>
                  </div>
                </div>
                <div className="col-sm-2">
                  <div className="single-widget">
                    <h2>Bağlantılar</h2>
                    <ul className="footer-widget-list">
                      <li>
                        <a href="#">
                          Milli
                          Eğitim
                          Bakanlığı
                        </a>
                      </li>
                      <li><a href="#">Yeğitek</a></li>
                      <li>
                        <a href="#">
                          Fatih
                          Projesi
                        </a>
                      </li>
                      <li><a href="#">UZEM</a></li>
                      <li><a href="#">UES</a></li>
                      <li><a href="#">eTwinning</a></li>
                    </ul>
                  </div>
                </div>
                <div className="col-sm-2">
                  <div className="single-widget">
                    <h2>Araçlar</h2>
                    <ul className="footer-widget-list">
                      <li>
                        <a href="#">
                          İçerik
                          Geliştirme
                          Araçları
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          Yardımcı
                          Programlar
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>
        {/*/Footer-End*/}
        <div id="c-mask" className="c-mask" />
        {/* jQuery */}
        {/* Bootstrap Core JavaScript */}
        {/* Plugins JavaScript */}
        {/* Masonry Javascript */}
        {/* Custom JavaScript */}
      </div>
    );
  }
}

function mapStateToProps(appState) {
  return {
    nUnreadMessages: appState.nUnreadMessages,
    nNotifications: getNotificationsString(appState),
    notificationsFetched: appState.notificationsFetched,
    photoUrl: appState.user.avatar
  };
}

const EbaKonteyner = connect(mapStateToProps)(EbaPure);

console.log("icerikler", store.getState().arama.icerikler);

export default class Main extends Component {
  render() {
    return (
      <Provider store={store}>
        <EbaKonteyner />
      </Provider>
    );
  }
}
