import { Provider, connect } from "react-redux";
import { createStore, combineReducers } from "redux";
import React, { Component } from "react";
import axios from "axios";

var mapStateToProps = function(state) {
  return {
    count: state.count
  };
};

var mapDispatchToProps = function(dispatch) {
  return {
    artir: () =>
      dispatch({
        type: "ARTIR"
      }),
    azalt: () =>
      dispatch({
        type: "AZALT"
      }),
    reset: () =>
      dispatch({
        type: "RESET"
      }),
    besArtir: () =>
      dispatch({
        type: "5_ARTIR"
      })
  };
};

class SafSayac extends Component {
  render() {
    return (
      <div>
        Sayacin
        degeri:
        {" "}
        {this.props.count}

        <button onClick={this.props.get}>
          Serverdan
          datayi
          al
        </button>

        <button onClick={this.props.artir}>
          Artir
        </button>

        <button onClick={this.props.azalt}>
          Azalt
        </button>

        <button onClick={this.props.reset}>
          Reset
        </button>
        <button onClick={this.props.besArtir}>
          5
          artir
        </button>
      </div>
    );
  }
}

function mapDispatchToPropsServer(dispatch) {
  return {
    artir: async function() {
      var x = await sayacApi.increase();
      dispatch(setSayacCreator(x));
    },

    reset: async function() {
      var oncekiDeger = store.getState().count;

      dispatch({
        type: "SET_SAYAC",
        payload: 0
      });

      dispatch({
        type: "FETCH_IN_PROGRESS"
      });
      try {
        var x = await sayacApi.reset();
        dispatch({
          type: "SET_SAYAC",
          payload: x.data.count
        });
      } catch (e) {
        dispatch({
          type: "SET_SAYAC",
          payload: oncekiDeger
        });
      }

      dispatch({
        type: "FETCH_COMPLETE"
      });
    },

    get: async function() {
      var x = await sayacApi.get();
      dispatch({
        type: "SET_SAYAC",
        payload: x.data.count
      });
    }
  };
}

var AkilliSayac = connect(mapStateToProps, mapDispatchToPropsServer)(SafSayac);

function sayacReducer(state = 0, action) {
  var newState = state;
  switch (action.type) {
    case "ARTIR":
      newState = state + 1;
      break;
    case "AZALT":
      newState = state - 1;
      break;
    case "RESET":
      newState = 0;
      break;

    case "SET_SAYAC":
      newState = action.payload;
      break;

    case "5_ARTIR":
      newState = state + 5;
      break;
  }

  return newState;
}

function loginFormReducer(
  state = {
    email: "",
    password: "",
    id: null
  },
  action
) {
  var newState = state;

  if (action.id !== state.id) {
    return state;
  }

  if (action.namespace !== "LOGIN_FORM") {
    return state;
  }

  switch (action.type) {
    case "SET_EMAIL":
      newState = {
        ...newState,
        email: action.payload
      };
      break;

    case "SET_PASSWORD":
      newState = {
        ...newState,
        password: action.payload
      };
      break;
  }

  return newState;
}

var appReducer = function(
  state = {
    count: 0,
    loginForm1: {
      email: "",
      password: "",
      id: 1
    },
    loginForm2: {
      email: "",
      password: "",
      id: 2
    }
  },
  action
) {
  return {
    count: sayacReducer(state.sayac, action),
    loginForm1: loginFormReducer(state.loginForm1, action),
    loginForm2: loginFormReducer(state.loginForm2, action)
  };
};

// new SayacReducer
// new LoginFormReducer
// new LoginFormReducer

var store = createStore(appReducer);

store.subscribe(() => {
  console.log(store.getState());
});

var debug = true;

function SafLogin(props) {
  return (
    <form>
      {debug && JSON.stringify(props)}

      <input
        onChange={props.onChangeEmail}
        value={props.email}
        placeholder="email"
      />
      <input
        onChange={props.onChangePassword}
        value={props.password}
        placeholder="password"
      />
    </form>
  );
}

function mapStateToPropsLogin1(state) {
  return state.loginForm1;
}

function mapStateToPropsLogin2(state) {
  return state.loginForm2;
}

function mapDispatchToPropsLoginGenerate(id) {
  return function mapDispatchToPropsLogin(_dispatch) {
    var baseAction = {
      id: id,
      namespace: "LOGIN_FORM"
    };

    var dispatch = function(x) {
      return _dispatch({
        ...x,
        ...baseAction
      });
    };

    return {
      onChangeEmail: function(e) {
        dispatch({
          ...baseAction,
          type: "SET_EMAIL",
          payload: e.target.value
        });
      },

      onChangePassword: function(e) {
        dispatch({
          type: "SET_PASSWORD",
          payload: e.target.value
        });
      }
    };
  };
}

var AkilliLoginForm1 = connect(
  mapStateToPropsLogin1,
  mapDispatchToPropsLoginGenerate(1)
)(SafLogin);
var AkilliLoginForm2 = connect(
  mapStateToPropsLogin2,
  mapDispatchToPropsLoginGenerate(2)
)(SafLogin);

export default class AnaBilesen extends Component {
  render() {
    return (
      <Provider store={store}>
        <div>
          <AkilliSayac />
          <AkilliLoginForm1 />
          <AkilliLoginForm2 />

        </div>
      </Provider>
    );
  }
}

// var hediyePaketi = fetch("http://localhost:1234/api/soru")

// hediyePaketi
//     .then(hediye => hediye.json())
//     .then(sonuc => console.log(sonuc));

function sleep(sure) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      resolve();
    }, sure);
  });
}

export var sayacApi = {
  async get() {
    await sleep(3000);

    // return {data: {count: 100}};

    return axios.get("http://localhost:1237/api/sayac");
  },

  increase() {
    return axios.post("http://localhost:1237/api/sayac");
  },

  reset() {
    return axios.delete("http://localhost:1237/api/sayac");
  }
};

async function sayaclaIlgiliFonksiyonlar() {
  var x = await sayacApi.get();
  console.log(x.data);

  await sayacApi.increase();
  await sayacApi.increase();
  await sayacApi.increase();

  x = await sayacApi.get();

  console.log(x.data);

  await sayacApi.reset();
}

sayaclaIlgiliFonksiyonlar();
