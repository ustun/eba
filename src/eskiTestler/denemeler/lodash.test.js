import _ from "lodash";

test("_", () => {
  console.log(_.sample(["ali", "ahmet", "mehmet"]));

  var x = _.times(10, () => "hello");

  console.log(x);

  var x = {
    y: {
      z: {
        t: {
          name: "ustun"
        }
      }
    }
  };

  console.log(
    _.get(x, "y.z.t.name"),
    _.get(x, ["y", "z", "t", "name"]),
    _.get(x, ["yzsdfsdf", "z", "t", "name"])
  );

  _.set(x, "y.z.t.name", "Ozgur");

  console.log(_.get(x, "y.z.t.name"));

  // ["name", "Ustun"],
  // ["surname", "Ozgur"],

  // {name: 'Ustun', surname: 'Ozgur'}

  console.log(_.snakeCase(_.kebabCase("ahmetDursun")));
});
