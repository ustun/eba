const Joi = require("joi");

describe("should do some validations", () => {
  it("should validate soruvalidator", done => {
    const soruSchema = Joi.object().keys({
      soru: Joi.string(),
      yanit: Joi.string().required()
    });

    Joi.validate(
      {
        soru: "NAsilsin?"
      },
      soruSchema,
      function(err, value) {
        // expect(2).toBe(3);
        expect(err).toBeTruthy();
        console.log(err);
        done();
      }
    );
  });

  it("should validate something", done => {
    const schema = Joi.object()
      .keys({
        username: Joi.string().alphanum().min(3).max(30).required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
        access_token: [Joi.string(), Joi.number()],
        birthyear: Joi.number().integer().min(1900).max(2013),
        email: Joi.string().email()
      })
      .with("username", "birthyear")
      .without("password", "access_token");

    // Return result.
    const result = Joi.validate(
      {
        username: "abc",
        birthyear: 1994
      },
      schema
    );
    // result.error === null -> valid

    // You can also pass a callback which will be called synchronously with the validation result.
    Joi.validate(
      {
        username: "abc",
        birthyear: 1994
      },
      schema,
      function(err, value) {
        expect(err).toBeFalsy();
        done();
      }
    ); // err === null -> valid
  });
});
