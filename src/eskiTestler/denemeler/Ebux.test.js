import { createStore } from "./Ebux";

describe("ebux", () => {
  it("should create a new store with a store reducer function", () => {
    var aReducerFn = function(state, action) {
      if (action.type === "ACILIS") {
        return {
          total: 0
        };
      }

      if (action.type == "TAHSILAT") {
        return {
          total: state.total + action.para
        };
      }

      if (action.type == "BORC") {
        return {
          total: state.total - action.para
        };
      }
    };

    var store = createStore(aReducerFn);

    expect(store).toBeTruthy();

    expect(store.getState()).toEqual({
      total: 0
    });

    store.dispatch({
      type: "TAHSILAT",
      para: 10
    });

    expect(store.getState()).toEqual({
      total: 10
    });

    store.dispatch({
      type: "TAHSILAT",
      para: 10
    });

    expect(store.getState()).toEqual({
      total: 20
    });

    store.dispatch({
      type: "BORC",
      para: 100
    });

    expect(store.getState()).toEqual({
      total: -80
    });
  });

  it("should have subscribers", () => {
    var aReducerFn = function(state, action) {
      if (action.type === "ACILIS") {
        return {
          total: 0
        };
      }

      if (action.type == "TAHSILAT") {
        return {
          total: state.total + action.para
        };
      }

      if (action.type == "BORC") {
        return {
          total: state.total - action.para
        };
      }
    };

    var store = createStore(aReducerFn);

    var x = 0;

    var abone1 = function() {
      console.log("Store degisti");
      x = x + 1;
    };

    var abone2 = function() {
      console.log("Store degisti ben de duydum aynisini");
    };

    store.subscribe(abone1);
    store.subscribe(abone2);

    store.dispatch({
      type: "TAHSILAT",
      para: 100
    });

    expect(x).toBe(1);

    expect(store.getState()).toEqual({
      total: 100
    });
  });
});
