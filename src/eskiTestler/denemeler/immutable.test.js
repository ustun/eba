import Immutable from "immutable";

describe("immutable", () => {
  test("map", () => {
    var x = Immutable.Map({
      isim: "Ustun"
    });

    expect(x.get("isim")).toBe("Ustun");

    var y = x.set("isim", "Ahmet");

    expect(x.get("isim")).toBe("Ustun");
    expect(y.get("isim")).toBe("Ahmet");
  });

  test("list", () => {
    var takimlar = Immutable.List.of("besiktas", "fener");

    expect(takimlar.count()).toBe(2);

    takimlar = takimlar.push("gs");

    expect(takimlar.count()).toBe(3);

    // expect(takimlar2.count()).toBe(3);

    var takimlar3 = takimlar.map(x => x.toUpperCase());

    expect(takimlar3.count()).toBe(3);

    console.log(takimlar3);
  });

  it("testing fromJS", () => {
    var insan = Immutable.fromJS({
      id: 1,
      hobiler: ["Dans", "Futbol"],
      isim: "Ustun"
    });

    console.log(insan);

    expect(insan.get("id")).toBe(1);

    var result = Immutable.is(
      insan.get("hobiler"),
      Immutable.List.of("Dans", "Futbol")
    );
    expect(result).toBe(true);
  });

  it("sayac reducer with immutable", () => {
    var initialState = Immutable.Map({
      count: 0
    });

    function sayacReducer(state = initialState, action) {
      if (action.type == "ARTIR") {
        return state.set("count", state.get("count") + 1);
      }

      if (action.type == "AZALT") {
        return state.set("count", state.get("count") - 1);
      }

      return state;
    }

    var sayac1 = sayacReducer(undefined, {
      type: "ARTIR"
    });

    expect(sayac1.get("count")).toBe(1);
  });

  it("immutable records", () => {
    var initialStateFn = Immutable.Record({
      count: 0
    });

    function sayacReducer(state = initialStateFn(), action) {
      console.log(state);
      if (action.type == "ARTIR") {
        return state.set("count", state.count + 1);
      }

      if (action.type == "AZALT") {
        return state.set("count", state.count - 1);
      }

      return state;
    }

    var sayac1 = sayacReducer(undefined, {
      type: "ARTIR"
    });

    expect(sayac1.get("count")).toBe(1);
  });
});
