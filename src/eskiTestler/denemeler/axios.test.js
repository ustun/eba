import axios from "axios";
import { sayacApi } from "./SayacCustomRedux";

describe("sayacApi calls", () => {
  it("should get", async done => {
    try {
      await sayacApi.reset();
      var x = await sayacApi.get();

      expect(x.data.count).toBe(0);
      expect(x).toBeTruthy();
      done();
    } catch (e) {
      console.log(e);
      done();
    }
  });
});

it("should do a GET request", done => {
  axios
    .get("http://localhost:1237/api/soru")
    .then(x => console.log(x.data.data))
    .then(_ => done())
    .catch(y => console.log(y, "hata oldu"));
});

it("should do a GET request with async", async done => {
  try {
    var x = await axios.get("http://localhost:1237/api/soru");
  } catch (e) {}

  console.log("x asenkron olarak geldi", x.data.data);
  done();
});

it("should do a POST request", done => {
  axios
    .post("http://localhost:1237/api/soru", {
      soru: "deneme"
    })
    .then(x => console.log(x.data.data))
    .then(_ => done())
    .catch(y => console.log(y, "hata oldu"));
});

it("should do a DELETE request", done => {
  axios
    .delete("http://localhost:1237/api/soru/2", {
      soru: "deneme"
    })
    .then(x => console.log(x.data.data))
    .then(_ => done())
    .catch(y => console.log(y, "hata oldu"));
});
