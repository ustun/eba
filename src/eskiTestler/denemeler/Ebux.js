export function createStore(aReducerFn) {
  var ilkAction = { type: "ACILIS" };

  var _state = aReducerFn({}, ilkAction);
  var _subscribers = [];

  return {
    getState() {
      return _state;
    },

    dispatch(action) {
      _state = aReducerFn(_state, action);

      // haber ver abonelere?

      _subscribers.forEach(aboneFonksiyonu => aboneFonksiyonu());
    },

    subscribe(aboneFonksiyonu) {
      _subscribers.push(aboneFonksiyonu);
    }
  };
}
