import React from "react";
import ReactDOM from "react-dom";
import {
  clearCompletedTodos,
  removeTodoFromList,
  filterTodos,
  nLeftTodos,
  newTodo,
  addNewTodo,
  toggleTodoInList,
  toggleTodo,
  aIleBaslayanlariBul,
  yeniOgrenci,
  sayaciArtir
} from "./App";
import App from "./App";
// it('renders without crashing', () => {
//     const div = document.createElement('div');
//     ReactDOM.render(<App />, div);
// });

// test('ogrenci olusturma', () => {

//     expect(yeniOgrenci('Ali', 1)).toEqual({isim: 'Ali', id: 1})

// });

// test('aritmetik islemleri', () => {

//     expect(2+1).toBe(3);

//     expect(2+2).toBe(4);

//     var ogrenciler = [
// 	yeniOgrenci('ali', 1),
// 	yeniOgrenci('ahmet', 2),
// 	yeniOgrenci('mehmet', 3)
//     ];

//     expect(aIleBaslayanlariBul(ogrenciler).length).toBe(2)

//     let ogrenciler2 = [
// 	{isim: "ali", id: 1},
// 	{isim: "Ahmet", id: 2},
// 	{isim: "mehmet", id: 3}
//     ];

//     expect(aIleBaslayanlariBul(ogrenciler2).length).toBe(2)

//     // let ogrenciler3 = [
//     // 	{isim: "ibrahim", id: 1},
//     // 	{isim: "İsmail", id: 2},
//     // 	{isim: "mehmet", id: 3}
//     // ];

//     // expect(aIleBaslayanlariBul(ogrenciler2).length).toBe(2)

// })

it("array fonksiyonlari", () => {
  var fruits = ["elma", "armut"];
  fruits.forEach(function(item, index, array) {
    expect(array).toBe(fruits);
  });

  fruits
    .filter(meyve => meyve.startsWith("e"))
    .forEach(function(item, index, array) {
      expect(item).toBe("elma");
      expect(index).toBe(0);

      // expect(array).toBe(['elma']);
    });

  // function myForEach() {

  // }

  // myForEach(fruits, function (item, index, array) {
  // 	expect(array).toEqual(fruits);
  // });

  function map(xler, donusumFonksiyonu) {
    var yler = [];

    for (var i = 0; i < xler.length; i++) {
      yler.push(donusumFonksiyonu(xler[i], i, xler));
    }

    return yler;
  }

  expect(map([0, 1, 2], x => x ** 3)).toEqual([0, 1, 8]);

  var benimHarikaFonksiyonum = function(x, i) {
    return i < 2 ? x ** 2 : x ** 3;
  };

  expect([0, 2, 2].map(benimHarikaFonksiyonum)).toEqual([0, 4, 8]);

  expect(map([0, 2, 2], benimHarikaFonksiyonum)).toEqual([0, 4, 8]);

  var kareAl = x => x ** 2;
  var kupAl = x => x ** 3;

  expect([0, 1, 2].map(kareAl)).toEqual([0, 1, 4]);
  expect(map([0, 1, 2], kareAl)).toEqual([0, 1, 4]);
  expect([0, 1, 2].map(kupAl)).toEqual([0, 1, 8]);
  expect(map([0, 1, 2], kupAl)).toEqual([0, 1, 8]);

  expect([3, 4, 5].reduce((x1, x2) => x1 + x2)).toBe(12);
  expect([3, 4, 5].reduce((x1, x2) => x1 * x2)).toBe(60);
  expect([3, 4, 5].reduce((x1, x2) => x1 * x2, 2)).toBe(120);

  expect(["Ali", "Ahmet", "Mehmet"].reduce((x1, x2) => x1 + " ve " + x2)).toBe(
    "Ali ve Ahmet ve Mehmet"
  );

  expect(["Ali", "Ahmet", "Mehmet"].join(" ve ")).toBe(
    "Ali ve Ahmet ve Mehmet"
  );

  expect(
    ["Ali", "Ahmet", "Mehmet"].reduce((x1, x2) => x1.concat([x2]), [])
  ).toEqual(["Ali", "Ahmet", "Mehmet"]);

  expect([1, 2, 3].reduce((x1, x2) => x1.concat([2 * x2]), [])).toEqual([
    2,
    4,
    6
  ]);

  function reduce(xler, reducer, ilkTohum) {
    var aku;

    // if (ilkTohum) {
    //     aku = reducer(ilkTohum, xler[0]);
    // } else {
    //     aku = xler[0];
    // }

    if (ilkTohum) {
      xler = [ilkTohum].concat(xler);
    }

    aku = xler[0];

    for (var i = 1; i < xler.length; i++) {
      var y = xler[i];

      aku = reducer(aku, y);
    }

    return aku;
  }

  expect(reduce([3, 4, 5], (x1, x2) => x1 + x2)).toBe(12);

  expect(reduce([3, 4, 5], (x1, x2) => x1 * x2, 2)).toBe(120);

  // [] 1
  // [2] 2 3
  // [2 4] 3
  // [2 4 6]

  var xler = [1, 2, 3];

  xler.push(4);

  expect(xler).toEqual([1, 2, 3, 4]);

  var z = xler.pop();
  expect(xler).toEqual([1, 2, 3]);

  expect(z).toBe(4);

  // splice, slice

  xler.splice(1, 1);

  expect(xler).toEqual([1, 3]);

  var z = xler.slice();

  expect(z).not.toBe(xler);
  expect(z).toEqual(xler);

  // [2, 43)

  var y = xler.slice(1, 2);
  3 + 2;

  xler.slice(1, 2);
  xler.slice(1, 2);
  xler.slice(1, 2);
  xler.slice(1, 2);

  expect(xler).toEqual([1, 3]);
  expect(y).toEqual([3]);

  // every

  expect([0, 2].every(x => x % 2 == 0)).toBe(true);
  expect([0, 2].every(x => x % 2 == 1)).toBe(false);

  expect([0, 1, 2].some(x => x % 2 == 1)).toBe(true);

  // some

  // [].concat(["Ali"])

  // [["Ali"], "Ahmet", "Mehmet"]
  // [["Ali", "Ahmet"], "Mehmet"]]
  // [["Ali", "Ahmet", "Mehmet"]]

  // ["Ali", "Ahmet", "Mehmet"]
});

test("obje ile ilgili", () => {
  var x = {
    name: "Ahmet",
    id: 1
  };

  expect(x.name).toBe("Ahmet");

  var name = "Ahmet";
  var id = 1;

  var y = {
    name: name,
    id: id
  };

  expect(y.name).toBe("Ahmet");

  var z = {
    name,
    id
  };

  expect(z.name).toBe("Ahmet");

  var t = {
    name: "Ahmet",
    id: 1
  };

  var name = t.name;
  var id = t.id;

  expect(name).toBe("Ahmet");
});

test("can structure", () => {
  var name = "Ali";
  var id = 3;

  var z = {
    name,
    id
  };

  expect(z.name).toBe("Ali");

  expect(z).toEqual({
    name: "Ali",
    id: 3
  });
});

test("can destructure", () => {
  var t = {
    name: "Mehmet",
    id: 1
  };

  var { name, id } = t;

  expect(name).toBe("Mehmet");
});

test("sayaci artirmayi test et", () => {
  expect(
    sayaciArtir(
      [
        {
          id: 1,
          value: 2
        },
        {
          id: 2,
          value: 3
        }
      ],
      2
    )
  ).toEqual([
    {
      id: 1,
      value: 1
    },
    {
      id: 2,
      value: 4
    }
  ]);

  expect(sayaciArtir([], 2)).toEqual([]);
});

describe("todo uygulamasi", () => {
  it("can create new todo", () => {
    expect(newTodo("Utu yap")).toEqual({
      tanim: "Utu yap",
      completed: false
    });
  });

  it("can add new todo", () => {
    expect(addNewTodo([newTodo("Utu yap")], newTodo("Yemek yap"))).toEqual([
      newTodo("Utu yap"),
      newTodo("Yemek yap")
    ]);
  });

  it("can toggle a single todo", () => {
    var x = newTodo("Utu yap");
    var y = toggleTodo(x); // {type: 'toggleTodo', icerik: 'utu yap'}
    expect(y).toEqual({
      completed: true,
      tanim: "Utu yap"
    });

    expect(x.completed).toBe(false);
    expect(y.completed).toBe(true);
  });

  it("can toggle a todo in a list", () => {
    expect(
      toggleTodoInList([newTodo("Utu yap"), newTodo("Yemek yap")], "Utu yap")
    ).toEqual([toggleTodo(newTodo("Utu yap")), newTodo("Yemek yap")]);

    var x = [newTodo("Utu yap"), newTodo("Yemek yap")];

    expect(toggleTodoInList(toggleTodoInList(x, "Utu yap"), "Utu yap")).toEqual(
      x
    );
  });

  it("can remove todo in a list", () => {
    expect(
      removeTodoFromList([newTodo("Utu yap"), newTodo("Yemek yap")], "Utu yap")
    ).toEqual([newTodo("Yemek yap")]);
  });

  it("can calculate number of todos left", () => {
    expect(nLeftTodos([newTodo("a"), newTodo("b")])).toBe(2);

    expect(nLeftTodos([newTodo("a"), newTodo("b"), newTodo("c")])).toBe(3);

    expect(
      nLeftTodos([newTodo("a"), newTodo("b"), toggleTodo(newTodo("c"))])
    ).toBe(2);
  });

  it("can get todos based on completed/not completed", () => {
    expect(
      filterTodos([newTodo("a"), newTodo("b"), toggleTodo(newTodo("c"))], "All")
        .length
    ).toBe(3);

    expect(
      filterTodos(
        [newTodo("a"), newTodo("b"), toggleTodo(newTodo("c"))],
        "Active"
      ).length
    ).toBe(2);

    expect(
      filterTodos(
        [newTodo("a"), newTodo("b"), toggleTodo(newTodo("c"))],
        "Completed"
      ).length
    ).toBe(1);
  });

  it("can clear completed", () => {
    expect(
      clearCompletedTodos([
        newTodo("a"),
        newTodo("b"),
        toggleTodo(newTodo("c"))
      ])
    ).toEqual([newTodo("a"), newTodo("b")]);
  });
});
