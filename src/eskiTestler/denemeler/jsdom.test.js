import jsdom, { jsdom as JSDOM } from "jsdom";

async function mockAPI() {
  throw new Error("something went wrong");
  //   return 43;
}

describe("js dom tests", () => {
  it("should do something", async () => {
    // console.log(Object.keys(jsdom));

    var x = JSDOM(`
<!doctype html>
<html>
<body>
<h1>header</h1>
<div>foo</div>
`);

    // console.log(global)
    console.log(Object.keys(global).sort().join("\n"));

    // fetch("http://127.0.0.1:8080")
    //     .then(response => console.log(response))
    //     .catch(error => console.log(error));

    x = await Promise.resolve(42);

    console.log(x);

    // x = await mockAPI();
    // console.log(x);

    alert("foo");
    // console.log(jsdom.serializeDocument(x))
    // console.log(jsdom.serializeDocument(x).length)

    // console.log(x.body.querySelector('div').textContent)
    // console.log(x.body.textContent)
    // console.log(x.body.querySelector('div').innerHTML)

    // console.log(x, Object.values(x), Object.keys(x), x.body, x.head)

    // for (var y in x) {
    //     ; console.log(y)
    // }

    expect(2).toBe(2);
  });
});
