// @flow

export default class Soru {
  constructor(name) {
    this.name = name;
  }

  isimUzunlugu() {
    return this.name.length;
  }

  kacSecenek2() {
    return 3;
  }
}
