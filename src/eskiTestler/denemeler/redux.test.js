import redux, { createStore } from "redux";
import { sayacReducer } from "./App";

test("the real redux with counter", () => {
  it("should create a store", () => {
    var sayacStore = createStore(sayacReducer);

    expect(sayacStore.getState()).toEqual({
      count: 0
    });

    sayacStore.dispatch({
      type: "ARTIR"
    });

    expect(sayacStore.getState()).toEqual({
      count: 1
    });

    sayacStore.dispatch({
      type: "ARTIR"
    });

    sayacStore.dispatch({
      type: "ARTIR"
    });

    expect(sayacStore.getState()).toEqual({
      count: 3
    });

    sayacStore.dispatch({
      type: "AZALT"
    });

    expect(sayacStore.getState()).toEqual({
      count: 2
    });

    sayacStore.dispatch({
      type: "RESET"
    });

    expect(sayacStore.getState()).toEqual({
      count: 0
    });
  });
});
