class Customer {
  constructor() {
    this.accounts = [];
  }
  addAccount(account) {
    this.accounts.push(account);
  }

  totalMoney() {
    return this.accounts.map(x => x.amount).reduce((a, b) => a + b);
  }
}

export default Customer;
