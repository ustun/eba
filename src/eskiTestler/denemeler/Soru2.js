export default class Soru {
  constructor(data) {
    this.secenekler = data.secenekler || [];
    this.soru = data.soru || null;
    this.yanit = data.yanit || null;
  }

  nSecenek() {
    return this.secenekler.length;
  }

  secenekVarMi(secenek) {
    return (
      this.secenekler.filter(_secenek => _secenek.metin === secenek.metin)
        .length > 0
    );
  }

  secenekEkle(secenek) {
    if (!this.secenekVarMi(secenek)) {
      this.secenekler.push(secenek);
    }
  }

  yanitBelirle(yanit) {
    if (this.secenekVarMi(yanit)) {
      this.yanit = yanit;
    } else {
      throw OlmayanSecenekError;
    }
  }

  isValid() {
    return this.nSecenek() >= 3 && this.yanit && this.secenekVarMi(this.yanit);
  }

  invalidationReasons() {
    const reasons = [];

    if (this.nSecenek() < 3) {
      reasons.push({
        metin: "Secenek sayisi az;"
      });
    }

    if (!this.yanit) {
      reasons.push({
        metin: "Yanit tanimli degil"
      });
    }

    if (this.yanit && !this.secenekVarMi(this.yanit)) {
      reasons.push({
        metin: "Yanit seceneklerde yok"
      });
    }

    return reasons;
  }
}

export var OlmayanSecenekError = new Error("Olmayan Secenek");
