export function sleep(sure) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      resolve();
    }, sure);
  });
}
