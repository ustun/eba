//// CHANGE SVG LOGO COLOR ////////////////////////////////

// $(function() {
//     jQuery('img.svg').each(function() {
//         var $img = jQuery(this);
//         var imgID = $img.attr('id');
//         var imgClass = $img.attr('class');
//         var imgURL = $img.attr('src');

//         jQuery.get(imgURL, function(data) {
//             // Get the SVG tag, ignore the rest
//             var $svg = jQuery(data).find('svg');

//             // Add replaced image's ID to the new SVG
//             if (typeof imgID !== 'undefined') {
//                 $svg = $svg.attr('id', imgID);
//             }
//             // Add replaced image's classes to the new SVG
//             if (typeof imgClass !== 'undefined') {
//                 $svg = $svg.attr('class', imgClass + ' replaced-svg');
//             }

//             // Remove any invalid XML tags as per http://validator.w3.org
//             $svg = $svg.removeAttr('xmlns:a');

//             // Check if the viewport is set, else we gonna set it if we can.
//             if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
//                 $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
//             }

//             // Replace image with new SVG
//             $img.replaceWith($svg);

//         }, 'xml');

//     });
// });

/////////////////////////////////////////////////////////
$(document).ready(function() {
    var $container = $('#search-content-results-items');
    $container.imagesLoaded(function() {
        $container.masonry({
            itemSelector: '.search-content-results-item',
            isFitWidth: true,
            isAnimated: false
        });
    });

    $('#detail-toggle-btn').click(function() {
        $('.package-content-comments').toggle(300, 'linear');
        $('.package-content-detail-button .fa-sort-desc').toggle(300, 'linear');
        $('.package-content-detail-button .fa-sort-asc').toggle(300, 'linear');
        $('.package-content-detail-topic span:last-child').toggle(300, 'linear');
        $('.package-content-detail-topic .seperator').toggle(300, 'linear');

    });
});